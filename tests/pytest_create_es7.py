import os

import pytest
from pytest_elasticsearch import factories
from unittest import mock

import naiad
from naiad.cli.create import parse_args, main


TEST_INDEX = 'e7_test_index'
CREATE_ARGS = ['--dims', 'time']


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"NAIAD_HOME": "."}, clear=True):
        yield


@pytest.fixture(autouse=True)
def store():
    factories.elasticsearch('elasticsearch_nooproc')
    return naiad.connect('ES7')


def test_create_index(store):
    if store.exists(TEST_INDEX):
        print('Deleting pre-existing index {}'.format(TEST_INDEX))
        store.delete_index(TEST_INDEX)
    store.create_index(TEST_INDEX, granule_only=False, dims=['time'])
    assert(store.exists(TEST_INDEX))
    store.delete_index(TEST_INDEX)


def test_create_already_existing_index(store):
    store.create_index(TEST_INDEX, granule_only=False, dims=['time'])
    with pytest.raises(naiad.stores.store.ExistingIndex):
        store.create_index(TEST_INDEX, granule_only=False, dims=['time'])
    store.delete_index(TEST_INDEX)


def test_create_config_command_args():
    connection_args = parse_args([
        '-sn', 'es7val', '--index', TEST_INDEX, *CREATE_ARGS])

    assert(connection_args.create_args.index == TEST_INDEX)
    assert(connection_args.create_args.dims == [CREATE_ARGS[1]])
    assert(connection_args.store.store_type == 'ES7')
    assert(connection_args.store.username == 'es7_login')


def test_create_default_command_args():
    connection_args = parse_args([
        '--index', TEST_INDEX, *CREATE_ARGS, 'ES7'])

    assert(connection_args.create_args.index == TEST_INDEX)
    assert(connection_args.create_args.dims == [CREATE_ARGS[1]])
    assert(connection_args.store.store_type == 'ES7')
    assert(connection_args.store.username is None)


def test_create_index_trajectory(store):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, *CREATE_ARGS, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    assert(store.exists(TEST_INDEX))
    store.delete_index(TEST_INDEX)


def test_create_index_in_unknown_store():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            '--index', TEST_INDEX, *CREATE_ARGS, 'ES7',
            '--url', 'https://dummyelastic.com:9200',
            '--login', 'dummy',
            '--password', 'dummy password',
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code != 0


def test_create_index_with_prefix():
    pstore = naiad.connect('ES7', prefix='myprefix_')
    if pstore.exists(TEST_INDEX):
        print('Deleting pre-existing index {}'.format(TEST_INDEX))
        pstore.delete_index(TEST_INDEX)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            '--index', TEST_INDEX, *CREATE_ARGS, 'ES7',
            '--prefix', 'myprefix_'
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    assert (pstore.exists(TEST_INDEX))
    pstore.delete_index(TEST_INDEX)
