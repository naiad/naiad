from pathlib import Path
import unittest

import naiad_commons
import naiad_commons.stores.store as store
import naiad_commons.data.tile as tile


TEST_INDEX = 'e6_test_index'

TEST_FILE = (
    '20200409035203-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_B'
    '-sstmgr_metop01_20200409_035203-v02.0-fv01.0.nc')


class TestElasticsearch6(unittest.TestCase):
    """Test class for Elasticsearch6 store

    An Elasticsearch service must be running, its URL defined in NAIAD_URL env
    var. If not, access to local service is attempted (http://localhost:9200)
    """
    def setUp(self):
        """ Setting up for the test """
        self.store = naiad_commons.connect('ES6')
        self.store.create_index(TEST_INDEX, granule_only=False)
        self.assertTrue(self.store.exists(TEST_INDEX))

    def test_index_exists(self):
        self.assertTrue(self.store.exists(TEST_INDEX))

    def test_create_already_existing_index(self):
        try:
            self.store.create_index(TEST_INDEX, granule_only=False)
        except Exception as e:
            self.assertIsInstance(e, store.ExistingIndex)

    def test_ingest_granules_only(self):
        granules, tiles = tile.GeoRecord.read_tiles(
            Path(__file__).parent.absolute() /
                Path('samples') / Path(TEST_FILE + '.tiles'),
            granule_only=True)
        self.store.register(TEST_INDEX, granules)

    def test_ingest_granules_and_tiles(self):
        granules, tiles = tile.GeoRecord.read_tiles(
            Path(__file__).parent.absolute() /
                Path('samples') / Path(TEST_FILE + '.tiles'))

        self.store.register(TEST_INDEX, granules, tiles)

    def test_granule_properties(self):
        prop = self.store.granule_properties(TEST_INDEX)
        self.assertIsInstance(prop, dict)

    def test_tile_properties(self):
        prop = self.store.segment_properties(TEST_INDEX)
        self.assertIsInstance(prop, dict)

    def test_granule_info(self):
        res = self.store.granule_info(
            TEST_INDEX, TEST_FILE)
        print(res)

    def tearDown(self):
        """Cleaning up after the test"""
        # remove the index
        self.store.delete_index(TEST_INDEX)
        self.assertFalse(self.store.exists(TEST_INDEX))
