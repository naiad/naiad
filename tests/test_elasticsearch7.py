from pathlib import Path
import time
import unittest

import shapely.geometry

import naiad
import naiad.stores.store as store
import naiad.data.georecord as gc


TEST_INDEX = 'e7_test_index'

TEST_FILE = (
    '20200409035203-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_B'
    '-sstmgr_metop01_20200409_035203-v02.0-fv01.0.nc')
TEST_FILE = \
    'samples/tiles/jason-3' \
    '/JA3_IPN_2PdP153_035_20200404_223602_20200404_233215.nc.tiles'
TEST_FILE_BATCH = 'samples/tiles/jason-3/*.tiles'


class TestElasticsearch7(unittest.TestCase):
    """Test class for Elasticsearch7 store

    An Elasticsearch service must be running, its URL defined in NAIAD_URL env
    var. If not, access to local service is attempted (http://localhost:9200)
    """
    store = None

    def setUp(self):
        """ Setting up for the test """
        self.store = naiad.connect('ES7')
        if self.store.exists(TEST_INDEX):
            self.store.delete_index(TEST_INDEX)
        self.store.create_index(TEST_INDEX, granule_only=False, dims=['time'])

    def test_index_exists(self):
        self.assertTrue(self.store.exists(TEST_INDEX))

    def test_create_already_existing_index(self):
        try:
            self.store.create_index(
                TEST_INDEX, granule_only=False, dims=['time'])
        except Exception as e:
            self.assertIsInstance(e, store.ExistingIndex)

    def test_ingest_granules_only(self):
        granule = gc.read_csv(
            Path(__file__).parent.absolute() / Path(TEST_FILE),
            granule_only=True)
        self.assertIsInstance(next(iter(granule)), gc.GeoRecord)
        self.store.register(TEST_INDEX, granule)

        time.sleep(2)
        res = self.store.granule_info(
            TEST_INDEX, Path(TEST_FILE).name.strip('.tiles'))
        self.assertIsInstance(res, gc.GeoRecord)

    def test_ingest_granules_and_tiles(self):
        granules = gc.read_csv(
            Path(__file__).parent.absolute() / Path(TEST_FILE))
        self.store.register(TEST_INDEX, granules)

        time.sleep(2)
        res = self.store.granule_info(
            TEST_INDEX, Path(TEST_FILE).name.strip('.tiles'))
        self.assertIsInstance(res, gc.GeoRecord)

    def test_granule_search(self):
        granules = gc.read_csv(
            Path(__file__).parent.absolute() / Path(TEST_FILE))
        self.store.register(TEST_INDEX, granules)

        time.sleep(2)
        # rough (granule search)
        total, res = self.store.search(
            [TEST_INDEX],
            area=shapely.geometry.box(-180, -90, 180, 90, ccw=True)
        )
        self.assertEqual(len(res[TEST_INDEX]), 1)
        self.assertIsInstance(res[TEST_INDEX][0], gc.GeoRecord)

        # rough search with errors
        total, res, err = self.store.search(
            [TEST_INDEX],
            area=shapely.geometry.box(-180, -90, 180, 90, ccw=True),
            return_errors=True
        )
        self.assertEqual(len(err), 0)

        # precise search
        total, res = self.store.search(
            [TEST_INDEX],
            area=shapely.geometry.box(-180, 0, 0, 90, ccw=True),
            add_slices=True
        )
        self.assertEqual(len(res[TEST_INDEX]), 1)
        print("AGG RES ", res['e7_test_index'][0])

    def test_granule_search_batch(self):
        for f in Path(__file__).parent.absolute().glob(TEST_FILE_BATCH):
            self.store.register(TEST_INDEX, gc.read_csv(f))

        time.sleep(2)
        # rough (granule search)
        total, res = self.store.search(
            [TEST_INDEX],
            area=shapely.geometry.box(-180, -90, 180, 90, ccw=True)
        )
        self.assertEqual(len(res[TEST_INDEX]), 26)
        self.assertIsInstance(res[TEST_INDEX][0], gc.GeoRecord)
        self.assertIsInstance(res[TEST_INDEX][0].subset_slices, type(None))

        # rough search with errors
        total, res, err = self.store.search(
            [TEST_INDEX],
            area=shapely.geometry.box(-180, -90, 180, 90, ccw=True),
            return_errors=True
        )
        self.assertEqual(len(err), 0)

        # precise search
        total, res = self.store.search(
            [TEST_INDEX],
            area=shapely.geometry.box(-180, -90, 180, 90, ccw=True),
            add_slices=True
        )
        print(res)

        self.assertEqual(len(res[TEST_INDEX]), 26)
        self.assertIsInstance(res[TEST_INDEX][0], gc.GeoRecord)
        self.assertIsInstance(res[TEST_INDEX][0].subset_slices['time'], slice)

    def test_granule_properties(self):
        prop = self.store.granule_properties(TEST_INDEX)
        self.assertIsInstance(prop, dict)

    def test_tile_properties(self):
        prop = self.store.segment_properties(TEST_INDEX)
        self.assertIsInstance(prop, dict)

    def test_granule_info_not_existing(self):
        res, err = self.store.granule_info(
            TEST_INDEX, Path(TEST_FILE).name.strip('.tiles'),
            fail_silently=True, return_errors=True)
        self.assertTrue(err[0][0] == 'granule not found')

    def tearDown(self):
        """Cleaning up after the test"""
        # remove the index
        self.store.delete_index(TEST_INDEX)
        self.assertFalse(self.store.exists(TEST_INDEX))
