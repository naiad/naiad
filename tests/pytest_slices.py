from itertools import groupby
from operator import itemgetter

import numpy as np

import pytest


@pytest.mark.parametrize(
    "tile_slices, expected",
    [
        ([{'row': slice(0, 20), 'cell': slice(0, 20)}], 1),
        ([{'row': slice(0, 20), 'cell': slice(0, 20)},
          {'row': slice(0, 20), 'cell': slice(20, 40)}], 1),
        ([{'row': slice(0, 20), 'cell': slice(0, 20)},
          {'row': slice(0, 20), 'cell': slice(30, 40)}], 2),
        ([{'row': slice(0, 20), 'cell': slice(0, 20)},
          {'row': slice(30, 40), 'cell': slice(30, 40)}], 2),
        ([{'row': slice(0, 20), 'cell': slice(0, 20)},
          {'row': slice(0, 20), 'cell': slice(0, 40)}], 1),
     ])
def test_grouping_slices(tile_slices, expected):
    # arrange list of slices per dimension and merge slices
    slices = {dim: [] for dim in tile_slices[0]}
    for tile in tile_slices:
        for dim, sl in tile.items():
            slices[dim].extend(np.r_[sl])

    # merge slices
    for dim, sl in slices.items():
        slices[dim] = set(sl)

    # find disjoint slices in each slicing dimension
    disj_slices = {dim: [] for dim in tile_slices[0]}
    for dim, indices in slices.items():
        for k, g in groupby(enumerate(indices), lambda x: x[0] - x[1]):
            group = list(map(itemgetter(1), g))
            disj_slices[dim].append(slice(group[0], group[-1] + 1))

    # group disjoint tiles
    def _indice_group(dim, sli):
        for i in range(len(disj_slices[dim])):
            if sli.start >= disj_slices[dim][i].start and \
                    sli.stop <= disj_slices[dim][i].stop:
                #return disj_slices[dim][i]
                return i

    subsets = {}
    for t in tile_slices:
        group = str({
            d: _indice_group(d, sl) for d, sl in t.items()})
        if group not in subsets:
            subsets[group] = []
        subsets[group].append(t)

    print(subsets, expected)
    for k, v in subsets.items():
        print({d: disj_slices[d][v] for d, v in eval(k).items()})

    assert len(subsets) == expected
