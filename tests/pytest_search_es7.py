import os
from pathlib import Path
from unittest import mock

import pytest
from pytest_elasticsearch import factories

from dateutil import parser
import shapely.geometry

import naiad
from naiad.cli.search import parse_args, main


TEST_INDEX = 'e7_test_index'
TEST_FILE = \
    'tests/samples/tiles/jason-3/test1' \
    '/JA3_IPN_2PdP153_035_20200404_223602_20200404_233215.nc.tiles'
TEST_FILE_DISJOINT = \
    'tests/samples/tiles/jason-3/test1' \
    '/JA3_IPN_2PdP153_012_20200404_010306_20200404_015919.nc.tiles'
TEST_FILE_BATCH = 'samples/tiles/jason-3/*.tiles'

TEST_AREA = '60,-20,180,20'
TEST_START = '2020-04-04'
TEST_END = '2020-04-05 12:00:00'


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"NAIAD_HOME": "."}, clear=True):
        yield


@pytest.fixture(autouse=True)
def es_index():
    factories.elasticsearch('elasticsearch_nooproc')
    store = naiad.connect('ES7')
    if store.exists(TEST_INDEX):
        print('Deleting pre-existing index {}'.format(TEST_INDEX))
        store.delete_index(TEST_INDEX)
    store.create_index(TEST_INDEX, granule_only=False, dims=['time'])

    return store


def test_search_config_command_args():
    search_args = parse_args([
        '-sn', 'es7val', '--index', TEST_INDEX])
    assert(search_args.index == TEST_INDEX)
    assert(search_args.store.store_type == 'ES7')
    assert(search_args.store.username == 'es7_login')


def test_search_es7_default_command_args():
    search_args = parse_args([
        '--index', TEST_INDEX, 'ES7'])
    assert(search_args.index == TEST_INDEX)
    assert(search_args.store.store_type == 'ES7')
    assert(search_args.store.username is None)


def test_search_es7_command_args():
    search_args = parse_args([
        '--index', TEST_INDEX, 'ES7'])
    assert(search_args.index == TEST_INDEX)
    assert(search_args.store.store_type == 'ES7')
    assert(search_args.store.username is None)


def test_search_es7_default_empty(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_search_es7_default_empty_area(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, '--area', TEST_AREA, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_search_es7_default_empty_timeframe(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, '--start', TEST_START, '--end',
              TEST_END, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_search_es7_default_single(es_index):
    es_index.register_from_files(TEST_INDEX, TEST_FILE)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_search_es7_default_prefix(es_index):
    es_index.register_from_files(TEST_INDEX, TEST_FILE)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, 'ES7', '--prefix', 'naiad_'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_search_es7_disjoint_track(es_index):
    es_index.register_from_files(TEST_INDEX, TEST_FILE_DISJOINT)

    res = es_index.search(
        [TEST_INDEX], conf=None,
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END))

    assert(len(res[TEST_INDEX]) == 1)

    res = es_index.search(
        [TEST_INDEX], conf=None,
        area=shapely.geometry.box(-180., -80., 60., 80.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        return_slices=True
    )
    # print("RES 1 ", res[TEST_INDEX][0])

    # two pieces of the same orbit are in the same area
    # by default does not check if the pieces are disjoint
    res = es_index.search(
        [TEST_INDEX], conf=None,
        area=shapely.geometry.box(-180., -80., 60., 80.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END))
    print(res[TEST_INDEX][0])
    assert(len(res[TEST_INDEX]) == 1)

    # two pieces of the same orbit are in the same area
    # check if the pieces are disjoint (return_disjoint = True)
    res = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -80., 60., 80.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        return_slices=True,
        merge_segments=True,
        return_disjoint=True)
    assert (len(res[TEST_INDEX]) == 1)
    assert (len(res[TEST_INDEX][0].segments) == 2)

def test_search_es7_granule(es_index):
    es_index.register_from_files(TEST_INDEX, TEST_FILE_DISJOINT)

    res = es_index.search(
        [TEST_INDEX], conf=None,
        area=None,
        granules=Path(TEST_FILE_DISJOINT).name.strip(".tiles"),
        start=None,
        end=None)

    assert(len(res[TEST_INDEX]) == 1)

    res = es_index.search(
        [TEST_INDEX], conf=None,
        area=shapely.geometry.box(-180., -60., 180., 0.),
        granules=Path(TEST_FILE_DISJOINT).name.strip(".tiles"),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END))

    assert(len(res[TEST_INDEX]) == 1)

    res = es_index.search(
        [TEST_INDEX], conf=None,
        area=shapely.geometry.box(-180., -60., 180., 0.),
        granules=Path(TEST_FILE_DISJOINT).name.strip(".tiles"),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        return_slices=True)

    assert(len(res[TEST_INDEX]) == 1)
