#!/usr/bin/env python
"""

test the tile registration into Naiad


:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os
import sys
import glob
import unittest

from elasticsearch import Elasticsearch
  
from naiad.queries.index import Index
from naiad.processing.tile import Tile


class SimpleTestCase(unittest.TestCase):
        
    # preparing to test
    def setUp(self):
        """ Setting up for the test """
        print("TileRegistrationTest:setUp_:begin")
        self.CLASS_PATH = os.path.abspath(sys.modules[self.__module__].__file__)
        if os.environ.get('ES_URL') is not None:
            self.es = Elasticsearch(os.environ.get('ES_URL'))
        else:
            # use local elasticsearch
            self.es = Elasticsearch()
            

        # register some tiles
        print("TileRegistrationTest:setUp_:end")
     
    # ending the test
    def tearDown(self):
        """Cleaning up after the test"""
        print("TileRegistrationTest:tearDown_:begin")
        # remove the created index
        print("TileRegistrationTest:tearDown_:end")

    def test_index_creation(self):
        # create an index
        self.index1 = Index("iasi")
        self.index1.create(self.es)

    def test_tile_ingestion_no_bulk(self):
        # register tiles for IASI instrument
        files = glob.glob(os.path.join(self.CLASS_PATH,
                                       'samples/tiles/iasi/*.tiles'))
        
        for f in enumerate(files):
            if not os.path.exists(f):
                raise Exception("Can not read file : {}".format(f))
            print "Ingesting ", os.path.basename(f)

            tiles = []
            granules = []
            # open tile file and read records
            first = True
            for line in open(f, 'r'):
                # first line is the granule shape
                if first:
                    tile = Tile.decode_csv(line)
                    granules.append(tile)
                    first = False
                else:
                    tiles.append(Tile.decode_csv(line))
            
            self.index1.register_granules_and_tiles_bulk(
                self.es, 
                granules,
                tiles)

    def test_tile_ingestion_bulk(self):
        # register tiles for IASI instrument
        files = glob.glob(os.path.join(self.CLASS_PATH,
                                       'samples/tiles/aatsr/*.tiles'))

        tiles = []
        granules = []        
        for f in enumerate(files):
            if not os.path.exists(f):
                raise Exception("Can not read file : {}".format(f))
            print "Ingesting ", os.path.basename(f)

            # open tile file and read records
            first = True
            for line in open(f, 'r'):
                # first line is the granule shape
                if first:
                    tile = Tile.decode_csv(line)
                    granules.append(tile)
                    first = False
                else:
                    tiles.append(Tile.decode_csv(line))
        
        self.index.register_granules_and_tiles_bulk(
            self.es,
            granules,
            tiles)
        

if __name__ == '__main__':
    unittest.main()