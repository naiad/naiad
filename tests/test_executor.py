#!/usr/bin/env python
"""

test the executor module


:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os
import inspect
import unittest

from naiad.utils.executor import Executor


class TestExecutor(unittest.TestCase):
        
    # preparing to test
    def setUp(self):
        """ Setting up for the test """
        toolstore = os.path.join(
            os.path.dirname(inspect.getfile(self.__class__)),
            'samples/test_toolstore.cfg'
            )
        print "TOOLSTORE", toolstore
        self.executor = Executor(toolstore)

    # ending the test
    def tearDown(self):
        """Cleaning up after the test"""
        pass

    def test_get_ncview_command_with_multiple_granules(self):
        """retrieve the command line for ncview with multiple files as input"""
        granules = [
            "/my/data/store/file1.nc",
            "/my/data/store/file2.nc",
            "/my/data/store/file3.nc",
            "/my/data/store/file4.nc"           
        ]
        command = self.executor.get_command("ncview", granules)
        print command
        expected = "ncview " + " ".join(granules)
        print "Expected command ", expected

        self.assertEqual(command,
                         expected,
                         "The returned command is incorrect"
                         )
        
    def test_get_ncview_command_with_multiple_granules_and_with_default_toolstore(self):
        """retrieve the command line by creating a default toolstore file in
        home directory"""
        self.executor = Executor()
        self.test_get_ncview_command_with_multiple_granules()

    def test_get_ncview_command_with_multiple_granules_and_with_env_variable(self):
        """retrieve the command line using toolstore file in $NAIAD_TOOLSTORE env 
        variable"""
        os.environ['NAIAD_TOOLSTORE'] = os.path.join(
            os.path.dirname(inspect.getfile(self.__class__)),
            'samples/test_toolstore.cfg'
            )
        self.executor = Executor()
        self.test_get_ncview_command_with_multiple_granules()
