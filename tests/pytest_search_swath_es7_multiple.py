import os
from pathlib import Path
from unittest import mock

import pytest
from pytest_elasticsearch import factories

from dateutil import parser
import shapely.geometry

import naiad
from naiad.cli.search import parse_args, main


TEST_INDEX = 'e7_test_index_swath'
TEST_FILE = 'samples/tiles/avhrr_sst_metop_a-osisaf-l2p-v1.0/20150719232803-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_A-sstmgr_metop02_20150719_232803-v02.0-fv01.0.nc.tiles'
TEST_FILE_BATCH = Path('samples/tiles/avhrr_sst_metop_a-osisaf-l2p-v1.0/')

TEST_AREA = '-180,-60,180,0'
TEST_START = '2015-07-19'
TEST_END = '2015-07-20'


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"NAIAD_HOME": "."}, clear=True):
        yield


@pytest.fixture(autouse=True)
def es_index():
    factories.elasticsearch('elasticsearch_nooproc')
    store = naiad.connect('ES7')
    if store.exists(TEST_INDEX):
        print('Deleting pre-existing index {}'.format(TEST_INDEX))
        store.delete_index(TEST_INDEX)
    store.create_index(TEST_INDEX, granule_only=False, dims=['row', 'cell'])

    return store


def test_search_config_command_args():
    search_args = parse_args([
        '-sn', 'es7val', '--index', TEST_INDEX])
    assert(search_args.index == TEST_INDEX)
    assert(search_args.store.store_type == 'ES7')
    assert(search_args.store.username == 'es7_login')


def test_search_es7_default_command_args():
    search_args = parse_args([
        '--index', TEST_INDEX, 'ES7'])
    assert(search_args.index == TEST_INDEX)
    assert(search_args.store.store_type == 'ES7')
    assert(search_args.store.username is None)


def test_search_es7_command_args():
    search_args = parse_args([
        '--index', TEST_INDEX, 'ES7'])
    assert(search_args.index == TEST_INDEX)
    assert(search_args.store.store_type == 'ES7')
    assert(search_args.store.username is None)


def test_search_es7_default_empty(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_search_es7_default_empty_area(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, f'--area={TEST_AREA}', 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_search_es7_default_empty_timeframe(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, '--start', TEST_START, '--end',
              TEST_END, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_search_es7_default_single(es_index):
    es_index.register_from_files(TEST_INDEX, TEST_FILE)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_verify_search_result(es_index):
    es_index.register_from_files(
        TEST_INDEX, list(TEST_FILE_BATCH.glob("*.tiles")))

    res1 = es_index.search(
        [TEST_INDEX],
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END))

    assert(len(res1[TEST_INDEX]) == len(list(TEST_FILE_BATCH.glob("*.tiles"))))

    # with geographical area
    res2 = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END))
    assert (len(res2[TEST_INDEX]) < len(res1[TEST_INDEX]))

    # reduce wrt intersection ratio
    res3 = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        min_intersection=50.
    )
    assert (len(res3[TEST_INDEX]) < len(res2[TEST_INDEX]))
    assert(res3[TEST_INDEX][0].subsets is None)

    # return intersection area
    res4 = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        min_intersection=50.,
        return_intersection=True
    )
    assert (len(res4[TEST_INDEX]) < len(res2[TEST_INDEX]))
    assert(res4[TEST_INDEX][0].subsets is not None)

    # add slices
    res5 = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        min_intersection=50.,
        return_intersection=True,
        return_slices=True
    )
    assert (len(res5[TEST_INDEX]) == len(res4[TEST_INDEX]))
    assert(res5[TEST_INDEX][0].subsets is not None)

    # excludes some granules, w/o slices
    res6 = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        excluded_granules=[
            res5[TEST_INDEX][0].granule, res5[TEST_INDEX][1].granule],
        min_intersection=50.,
        return_intersection=True,
    )
    assert (len(res6[TEST_INDEX]) == len(res5[TEST_INDEX])-2)

    res6 = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        excluded_granules=[
            res5[TEST_INDEX][0].granule, res5[TEST_INDEX][1].granule],
        min_intersection=50.,
        return_intersection=True,
        return_slices=True
    )
    assert (len(res6[TEST_INDEX]) == len(res5[TEST_INDEX])-2)

    # return errors
    res4 = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        min_intersection=50.,
        return_intersection=True,
        return_errors=True
    )
    granules, errors = res4

    assert (len(granules[TEST_INDEX]) < len(res2[TEST_INDEX]))
    assert(len(errors) == 0)

    res4 = es_index.search(
        [TEST_INDEX],
        area=shapely.geometry.box(-180., -60., 180., 0.),
        start=parser.parse(TEST_START),
        end=parser.parse(TEST_END),
        min_intersection=50.,
        return_intersection=True,
        return_errors=True,
        return_slices=True
    )
    granules, errors = res4

    assert (len(granules[TEST_INDEX]) < len(res2[TEST_INDEX]))
    assert(len(errors) == 0)
