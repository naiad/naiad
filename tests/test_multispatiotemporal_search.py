import datetime
import shapely.geometry

from naiad.queries.server import Server
from naiad.queries.search import MultiSpatioTemporalQuery

es = Server()

targets = [
    (datetime.datetime(2019, 9, 27, 22, 30), shapely.geometry.box(-135, -71, -30, 60)),
    (datetime.datetime(2019, 10, 1, 22, 30),
     shapely.geometry.box(-135, -71, -30, 60)),
    (datetime.datetime(2019, 10, 3, 22, 30),
     shapely.geometry.box(-135, -71, -30, 60)),
]

msearch = MultiSpatioTemporalQuery(
    ['cersat_naiad_ascat_a_l2b_12km_knmi'],
    targets,
    delta_time=datetime.timedelta(hours=1)
)
res = msearch.run(es, precise=True)

for k, v in res.items():
    print(k)
    for t in v:
        print(str(t))
        print()
        t.show()

    print()
