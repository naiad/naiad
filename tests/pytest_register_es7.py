import os
from unittest import mock

import pytest
from pytest_elasticsearch import factories

import naiad
from naiad.cli.storing import parse_args, main


TEST_INDEX = 'e7_test_index'
TEST_FILE = \
    'samples/tiles/jason-3' \
    '/JA3_IPN_2PdP153_035_20200404_223602_20200404_233215.nc.tiles'
TEST_FILE_BATCH = 'samples/tiles/jason-3/*.tiles'


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"NAIAD_HOME": "."}, clear=True):
        yield


@pytest.fixture(autouse=True)
def es_index():
    factories.elasticsearch('elasticsearch_nooproc')
    store = naiad.connect('ES7')
    if store.exists(TEST_INDEX):
        print('Deleting pre-existing index {}'.format(TEST_INDEX))
        store.delete_index(TEST_INDEX)
    store.create_index(TEST_INDEX, granule_only=False, dims=['time'])


def test_register_config_command_args():
    connection_args = parse_args([
        '-sn', 'es7val', '--index', TEST_INDEX, '--records', TEST_FILE])
    assert(connection_args.index == TEST_INDEX)
    assert(connection_args.records == TEST_FILE)
    assert(connection_args.registration.store_type == 'ES7')
    assert(connection_args.registration.username == 'es7_login')


def test_register_es7_default_command_args():
    connection_args = parse_args([
        '--index', TEST_INDEX, '--records', TEST_FILE, 'ES7'])
    assert(connection_args.index == TEST_INDEX)
    assert(connection_args.records == TEST_FILE)
    assert(connection_args.registration.store_type == 'ES7')
    assert(connection_args.registration.username is None)


def test_register_es7_command_args():
    connection_args = parse_args([
        '--index', TEST_INDEX, '--records', TEST_FILE, 'ES7', '--optimize'])
    assert(connection_args.index == TEST_INDEX)
    assert(connection_args.records == TEST_FILE)
    assert(connection_args.registration.store_type == 'ES7')
    assert(connection_args.registration.username is None)
    assert connection_args.registration.optimize


def test_register_es7_default(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, '--records', TEST_FILE, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_register_es7_bulk(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, '--records', TEST_FILE, '--bulk',
              '--verbose', 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_register_es7_bulk_prefix(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, '--records', TEST_FILE, '--bulk',
              '--verbose', 'ES7', '--prefix', 'naiad_'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_register_es7_bulk_optimize(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--index', TEST_INDEX, '--records', TEST_FILE, '--bulk',
              '--verbose', 'ES7', '--optimize'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0
