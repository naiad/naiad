import os
from unittest import mock

import pytest
from pytest_elasticsearch import factories

import naiad
from naiad.cli.inquire import parse_args, main


TEST_INDEX = 'e7_test_index'


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"NAIAD_HOME": "."}, clear=True):
        yield


@pytest.fixture(autouse=True)
def es_index():
    factories.elasticsearch('elasticsearch_nooproc')
    store = naiad.connect('ES7')
    if store.exists(TEST_INDEX):
        print('Deleting pre-existing index {}'.format(TEST_INDEX))
        store.delete_index(TEST_INDEX)
    store.create_index(TEST_INDEX, granule_only=False, dims=['time'])


def test_inquire_config_command_args():
    connection_args = parse_args(['-sn', 'es7val', '--list'])
    assert(connection_args.inquire_args.command == 'list')


def test_inquire_default_command_args():
    connection_args = parse_args(['ES7'])
    assert(connection_args.inquire_args.command == 'list')


def test_inquire_properties_command_args():
    connection_args = parse_args(['--properties', '--index', TEST_INDEX, 'ES7'])
    assert(connection_args.inquire_args.command == 'properties')


def test_exists_es7_index(es_index):
    store = naiad.connect('ES7')
    assert(store.exists(TEST_INDEX))


def test_inquire_list_with_es7_index(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--list', 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_inquire_list_with_es7_index_with_prefix(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--list', '--prefix', 'dummy', 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_inquire_with_es7_granule_properties(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--properties', '--index', TEST_INDEX, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_inquire_with_es7_tile_properties(es_index):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main(['--tile-properties', '--index', TEST_INDEX, 'ES7'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0
