from datetime import datetime, timedelta
import glob
import os
from unittest import mock

import pytest
from pytest_elasticsearch import factories
import shapely.geometry
import matplotlib.pyplot as plt

from naiad_commons.data.geoindexrecord import FeatureIndexRecord
import naiad


TEST_INDEX_S6 = 'e7_test_index_s6'
TEST_FILES_S6 = glob.glob('tests/samples/tiles/sentinel-6/*.idx')

TEST_INDEX_S3A = 'e7_test_index_s3a'
TEST_FILES_S3A = glob.glob('tests/samples/tiles/sentinel-3_a/*.idx')

TEST_AREA = [-180, -90, 180, 90]


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"NAIAD_HOME": "."}, clear=True):
        yield


@pytest.fixture(autouse=True)
def store():
    factories.elasticsearch('elasticsearch_nooproc')
    store = naiad.connect('ES7')

    if store.exists(TEST_INDEX_S6):
        print('Deleting pre-existing index {}'.format(TEST_INDEX_S6))
        store.delete_index(TEST_INDEX_S6)
    store.create_index(TEST_INDEX_S6, granule_only=False, dims=['time'])
    store.register_from_files(TEST_INDEX_S6, TEST_FILES_S6)

    if store.exists(TEST_INDEX_S3A):
        print('Deleting pre-existing index {}'.format(TEST_INDEX_S3A))
        store.delete_index(TEST_INDEX_S3A)
    store.create_index(TEST_INDEX_S3A, granule_only=False, dims=['time'])
    store.register_from_files(TEST_INDEX_S3A, TEST_FILES_S3A)

    return store


def test_crossover_search_granule(store):

    assert(store.exists(TEST_INDEX_S6))

    res = store.crossover_search(
            TEST_INDEX_S6,
            crossed_index=TEST_INDEX_S3A,
            level='granule',
            area=shapely.geometry.box(*TEST_AREA),
            start=datetime(1950, 1, 1),
            end=datetime.now(),
            time_window=timedelta(hours=2))

    print('--------------------------------')
    ax = None
    for g, crossover in res:
        print('===> ', g)
        if ax is None:
            ax = FeatureIndexRecord.draw(g.geometry, colour='b')
        else:
            print("GEOM ", g.geometry)
            FeatureIndexRecord.draw(g.geometry, ax=ax, colour='b')
        print()
    print('--------------------------------')


def test_crossover_search_granule_precise(store):

    assert(store.exists(TEST_INDEX_S6))

    start = datetime.now()
    res = store.crossover_search(
            TEST_INDEX_S6,
            crossed_index=TEST_INDEX_S3A,
            level='granule',
            area=shapely.geometry.box(*TEST_AREA),
            start=datetime(2022, 8, 5),
            end=datetime(2022, 8, 6),
            time_window=timedelta(hours=2),
            return_slices=True
    )
    end = datetime.now()
    print("Nb res ", len(res), end - start)

    # Brute force colocation
    from naiad_commons.data.geoindexrecord import read_csv
    start = datetime.now()
    s6_granules = []
    for f in TEST_FILES_S6:
        s6_granules.append(read_csv(f))
    print(s6_granules)

    s3_granules = []
    for f in TEST_FILES_S3A:
        s3_granules.append(read_csv(f))
    print(s3_granules)

    n_colocs = 0
    for s6g in s6_granules:
        for s3g in s3_granules:
            for seg6 in s6g.segments:
                for seg3 in s3g.segments:
                    if seg6.geometry.intersects(seg3.geometry):
                        if seg6.end + timedelta(hours=2) <= seg3.start:
                            continue
                        if seg6.start - timedelta(hours=2) >= seg3.end:
                            continue
                        print(s6g.granule, " <=> ", s3g.granule)
                        print(seg6.start, seg6.end, seg3.start, seg3.end)
                        n_colocs += 1
    end = datetime.now()
    print('Brute coloc ', n_colocs, end - start)

    assert(n_colocs == len(res))

    print('--------------------------------')
    ax = None
    for crossover in res:
        print('===> ', crossover.reference.segments)
        if ax is None:
            ax = FeatureIndexRecord.draw(
                 crossover.reference.segments[0].geometry, colour='b')
        else:
            FeatureIndexRecord.draw(
                crossover.reference.segments[0].geometry, ax=ax, colour='b')
        print('SEGMENT ', crossover.match)
        FeatureIndexRecord.draw(
            crossover.match.segments[0].geometry, ax=ax, colour='r')
        print()
    print('--------------------------------')

    plt.show()


def test_crossover_search_command(store):
    from naiad.cli.crosssearch import main
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            '--cross', TEST_INDEX_S6,
            '--versus', TEST_INDEX_S3A,
            '--time-window', '120',
            '--return-slices',
            '--output-format', 'json',
            'ES7'
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0
