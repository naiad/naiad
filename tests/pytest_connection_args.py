import os
from unittest import mock

import pytest

from naiad.cli.inquire import parse_args


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"NAIAD_HOME": "."}, clear=True):
        yield


def test_connection_with_config():
    connection_args = parse_args(['-sn', 'es7val', '--list'])
    assert(connection_args.store.username == 'es7_login')


def test_connection_with_es7_args():
    connection_args = parse_args([
        'ES7', '--url', 'https://elasticsearch7-val.mycompany.fr:9200',
        '--login', 'mylogin', '--password', 'mypassword'
    ])
    assert(connection_args.store.username == 'mylogin')

