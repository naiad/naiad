#!/usr/bin/env python
"""

test the filelocator module


:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os
import inspect
import unittest

from naiad.utils.filelocator import FileLocator


class TestFileLocator(unittest.TestCase):
        
    # preparing to test
    def setUp(self):
        """ Setting up for the test """
        datastore = os.path.join(
            os.path.dirname(inspect.getfile(self.__class__)),
            'samples/test_datastore.cfg'
            )
        print "DATASTORE", datastore
        self.filelocator = FileLocator(datastore)

    # ending the test
    def tearDown(self):
        """Cleaning up after the test"""
        pass

    def test_get_full_path(self):
        """retrieve the full path of a granule using user specified datastore file"""
        granule = "S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3"
        dataset = "S3A_SL_1_RBT____NR"
        print "Granule : ", granule
        print "Dataset id : ", dataset
        expected = os.path.join("/my/data/store/2016/289", granule)
        print "Expected path ", expected
        predicted = self.filelocator.get_full_path(granule, dataset)
        print "Predicted path ", predicted
        self.assertEqual(predicted,
                         expected,
                         "The returned file path is incorrect"
                         )
        
    def test_get_full_path_with_default_datastore(self):
        """retrieve the full path of a granule by creating a default datastore file in home directory"""
        self.filelocator = FileLocator()
        self.test_get_full_path()

    def test_get_full_path_with_env_variable(self):
        """retrieve the full path of a granule using datastore file in $NAIAD_DATASTORE env variable"""
        os.environ['NAIAD_DATASTORE'] = os.path.join(
            os.path.dirname(inspect.getfile(self.__class__)),
            'samples/test_datastore.cfg'
            )
        self.filelocator = FileLocator()
        self.test_get_full_path()
