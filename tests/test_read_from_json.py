import unittest

import naiad


class ReadFromJson(unittest.TestCase):
    def test_read_from_json(self):
        json_file = 'crossover.json'
        result = naiad.from_json(json_file)
        self.assertIsInstance(result, list)
        pair = result[0]
        self.assertIsInstance(pair[0], naiad.data.tile.GeoRecord)
        self.assertIsInstance(pair[1], naiad.data.tile.GeoRecord)


if __name__ == '__main__':
    unittest.main()
