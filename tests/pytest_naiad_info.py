import pytest



@pytest.fixture
def args(tmpdir, request):
    insitu, count = insitu_parquet(tmpdir, request.param)

    manifest = Path('output/workspace/manifest') / (
            request.param.name + '.manifest')

    yield ['-p', str(arg_file(tmpdir, request.param)),
           '-c', str(config_file(tmpdir, insitu))], count, manifest

    shutil.rmtree(str(tmpdir))


def test_extraction_single_swath(args):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        naiad_info(args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    assert manifest.exists()
    assert int(check_output(["wc", "-l", manifest]).split()[0]) >= matchup_count
