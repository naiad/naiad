import glob
import os
from unittest import mock

import pytest
from pytest_elasticsearch import factories
import shapely.geometry

import naiad


TEST_INDEX = 'e7_test_index'
TEST_FILE = [
    'tests/samples/tiles/jason-3'
    '/test1/JA3_IPN_2PdP153_035_20200404_223602_20200404_233215.nc.tiles']
TEST_FILE_BATCH = glob.glob('tests/samples/tiles/jason-3/*.tiles')

TEST_AREA = '60,-20,180,20'
TEST_START = '2020-04-04'
TEST_END = '2020-04-05 12:00:00'


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"NAIAD_HOME": "."}, clear=True):
        yield


@pytest.fixture(autouse=True)
def store():
    factories.elasticsearch('elasticsearch_nooproc')
    store = naiad.connect('ES7')
    if store.exists(TEST_INDEX):
        print('Deleting pre-existing index {}'.format(TEST_INDEX))
        store.delete_index(TEST_INDEX)
    store.create_index(TEST_INDEX, granule_only=False, dims=['time'])
    store.register_from_files(TEST_INDEX, TEST_FILE)

    return store


def test_multisearch_granule(store):
    assert(store.exists(TEST_INDEX))
    res = store.multisearch(
        TEST_INDEX,
        level="granule",
        area=[
            shapely.geometry.box(-180., -90., 0., 0.),
            shapely.geometry.box(-180., 0., 0., 90.),
            shapely.geometry.box(0., -90., 180., 0.),
            shapely.geometry.box(0., 0., 180., 90.),
        ]
    )
    for items in res:
        for intersect in items:
            print(intersect)


def test_multisearch_granule_with_offsets(store):
    assert(store.exists(TEST_INDEX))
    res = store.multisearch(
        TEST_INDEX,
        level="granule",
        area=[
            shapely.geometry.box(-180., -90., 0., 0.),
            shapely.geometry.box(-180., 0., 0., 90.),
            shapely.geometry.box(0., -90., 180., 0.),
            shapely.geometry.box(0., 0., 180., 90.),
        ],
        return_slices=True
    )
    for items in res:
        for intersect in items:
            print("INTERSECT", intersect)
