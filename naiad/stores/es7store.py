# -*- coding: utf-8 -*-
"""
Interface to the Elasticsearch v7 index storage

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import asyncio
import copy
import json
import logging
import typing as T
from datetime import datetime, timedelta
from itertools import groupby
from operator import itemgetter, attrgetter
from pathlib import Path

import dateutil.parser
import elasticsearch
import elasticsearch.helpers
import numpy as np
import shapely.geometry
import shapely.ops
from elasticsearch import AsyncElasticsearch
from naiad_commons.data.geoindexrecord import FeatureIndexRecord, \
    SegmentIndexRecord, Crossover, read_csv

import naiad.stores.store as store

# type hint aliases
Area = T.Union[shapely.geometry.Polygon, shapely.geometry.MultiPolygon]
Constraint = T.List[T.Tuple[str, str, str]]
MSearchResult = T.Union[
    T.List[T.List[FeatureIndexRecord]],
    T.Tuple[
        T.List[T.List[FeatureIndexRecord]],
        T.List[T.List[T.Tuple[str, str]]]
    ],
]

WORLD_WEST0 = shapely.geometry.Polygon([
    (-180., -90.), (0., -90), (0., 90.), (-180., 90), (-180., -90.)])
WORLD_EAST0 = shapely.geometry.Polygon([
    (0., -90.), (180., -90), (180., 90.), (0., 90), (0., -90.)])


INDEX_MAPPING = {
    "settings": {
        "index": {
            "number_of_shards": 5,
            "number_of_replicas": 1,
            "sort.field": "time_coverage_start",
            "sort.order": "asc"
        }
    },
    "mappings": {
        "_meta": {},
        "properties": {
            # granule or tile
            "level":  {
                "type": "keyword",
                "norms": "false"
            },
         #   "level": {
         #       "type": "join",
         #       "relations": {
         #           "granule": "tile"}},
            "geometry": {
                "type": "geo_shape",
              #  "tree": "quadtree",
              #  "precision": "1km",
            },
            "feature": {
                "type": "keyword",
                "index": "false",
                "norms": "false"
            },
            "granule": {
                "type": "keyword",
               # "index": "false",
               # "norms": "false"
            },
            "time_coverage_end": {
                "type": "date",
            },
            "time_coverage_start": {
                "type": "date",
            }
        }
    }
}

MAX_BUCKETS = 10000

# error messages
INCOMPLETE = 'missing results'
NOT_FOUND = 'granule not found'


# specific arguments for each Naiad store operation
class ES7StoreConfig(store.StoreConfig):
    prefix: str = 'naiad_'


class ES7IndexCreationConfig(store.IndexCreationConfig):
    shards: int = 5
    replica: int = 1


class ES7RegistrationConfig(store.RegistrationConfig):
    optimize: bool = False


class ES7Store(store.Store):
    """Naiad store for Elasticsearch v7.x

    Args:
        host (optional, str or list): ElasticSearch master node(s). If not
            provided, the object will search for ``NAIAD_ELASTICSEARCH``
            environment variable. If it is not set, it will assume
            localhost:9200 is to be used by default. It can also be a list
            of hosts.
    """
    def __init__(
        self,
        host=None,
        username=None,
        password=None,
        prefix='naiad_',
        verbosity=logging.WARNING,
        **kwargs
    ):
        super(ES7Store, self).__init__(
            host=host, username=username, password=password,
            verbosity=verbosity, **kwargs)
        self.prefix = prefix

    def default_url(self):
        return 'http://localhost:9200'

    def set_verbosity(self, level=logging.WARNING):
        super(ES7Store, self).set_verbosity(level)

        tracer = logging.getLogger('elasticsearch.trace')
        tracer.setLevel(level)
        tracer.addHandler(logging.StreamHandler())

    def connect(self, sniffing=False, **kwargs) -> AsyncElasticsearch:
        """Init the connection to Elasticsearch store"""
        if self.hosts[0].startswith('https://'):
            use_ssl = True
        else:
            use_ssl = False

        if self.login is not None:
            http_auth = (self.login, self.password)
        else:
            http_auth = None

        try:
            if sniffing:
                return AsyncElasticsearch(
                    hosts=self.hosts,
                    http_auth=http_auth,
                    use_ssl=use_ssl,
                    verify_certs=True,
                    # sniff before doing anything
                    sniff_on_start=True,
                    # refresh nodes after a node fails to respond
                    sniff_on_connection_fail=True,
                    # and also every 60 seconds
                    sniffer_timeout=60
                )
            else:
                return AsyncElasticsearch(
                    hosts=self.hosts,
                    http_auth=http_auth,
                    use_ssl=use_ssl,
                    verify_certs=True
                )
        except elasticsearch.exceptions.TransportError as e:
            raise store.StoreConnectionError(
                f'Could not connect to index store {self.hosts}', self.hosts)

    def close_connection(self) -> T.NoReturn:
        close_connection_loop = asyncio.get_event_loop()
        return close_connection_loop.run_until_complete(self.connection.close())

    async def _close_connection(self) -> T.NoReturn:
        await self.connection.close()

    def indices(self, prefix=None) -> T.List[str]:
        """Return the list of Naiad indices on the host"""
        indices_loop = asyncio.get_event_loop()
        return indices_loop.run_until_complete(self._indices(prefix=prefix))

    async def _indices(self, prefix=None) -> T.List[str]:
        if prefix is None:
            prefix = self.prefix
        resp = await self.connection.indices.get("{}*".format(prefix))
        return list(resp.keys())

    def exists(self, index: str, **kwargs) -> bool:
        """
        Check if an index is existing.

        Args:
            index (str): name of the index

        Returns:
            A boolean indicating whether the passed index exists
        """
        exists_loop = asyncio.get_event_loop()
        return exists_loop.run_until_complete(self._exists(index=index, **kwargs))

    async def _exists(self, index: str, **kwargs) -> bool:
        logging.debug(
            "Testing if index {} is existing in {}".format(
                index, self.connection))
        return await self.connection.indices.exists(self.prefix + index)

    async def _start_bulk_optimization(self, index):
        # indices = self.connection.indices

        # deactivate refresh
        await self.connection.indices.put_settings(
            body='{"index": {"refresh_interval" : "-1"}}',
            index=self.prefix + index,
            ignore_unavailable=True
        )

    def start_bulk_optimization(self, index):
        """Set an index configuration so that any subsequent bulk indexing will
        be more efficient. Don't forget to end this optimization state after
        the bulk ingestion is completed.

        The following optimizations are performed:
          * deactivate refresh_interval

        Args:
            index (str): name of the index

        """
        start_bulk_optimization_loop = asyncio.get_event_loop()
        start_bulk_optimization_loop.run_until_complete(self._start_bulk_optimization(index=index))

    async def _end_bulk_optimization(self, index):
        # refresh
        await self.connection.indices.put_settings(
            body='{"index": {"refresh_interval" : "30s"}}',
            index=self.prefix + index,
            ignore_unavailable=True
        )

        # And, a force merge should be called:
        await self.connection.indices.forcemerge(index=self.prefix + index,
                           max_num_segments=5,
                           #                           wait_for_merge=False,
                           request_timeout=120)

    def end_bulk_optimization(self, index):
        """End index configuration optimization for bulk indexing.

        Args:
            index (str): name of the index
        """
        end_bulk_optimization_loop = asyncio.get_event_loop()
        end_bulk_optimization_loop.run_until_complete(self._end_bulk_optimization(index=index))

    def create_index(
        self,
        name: str,
        dims: T.List[str],
        granule_properties: T.List[str] = None,
        segment_properties: T.List[str] = None,
        shards: int = 5,
        replica: int = 1,
        **kwargs):
        """Create an index in the store

        Args:
            dims: the names of the dimensions on which granules are subsetted
                to create tiles
        """
        create_index_loop = asyncio.get_event_loop()
        create_index_loop.run_until_complete(
            self._create_index(
                name=name,
                dims=dims,
                granule_properties=granule_properties,
                segment_properties=segment_properties,
                shards=shards,
                replica=replica,
                **kwargs
            )
        )

    async def _create_index(
        self,
        name: str,
        dims: T.List[str],
        granule_properties: T.List[str] = None,
        segment_properties: T.List[str] = None,
        shards: int = 5,
        replica: int = 1,
        **kwargs
    ):
        index = INDEX_MAPPING
        index['settings']['index']['number_of_shards'] = shards
        index['settings']['index']['number_of_replicas'] = replica

        # add dimensions for tiles
        if dims is not None:
            index['mappings']['_meta']['dims'] = dims
            for dim in dims:
                index['mappings']['properties'][dim + '_min'] = {
                    'type': 'integer',
                    'index': 'false'}
                index['mappings']['properties'][dim + '_max'] = {
                    'type': 'integer',
                    'index': 'false'}

        if granule_properties is None:
            granule_properties = {}
        if segment_properties is None:
            segment_properties = {}
        index['mappings']['_meta']['granule_properties'] = \
            list(granule_properties.keys())
        index['mappings']['_meta']['tile_properties'] = \
            list(segment_properties.keys())
        for properties in [granule_properties, segment_properties]:
            for prop in properties:
                pname, ptype = prop
                index['mappings']['properties'][pname] = {
                    'type': ptype,
                    'index': 'false'}

        try:
            await self.connection.indices.create(
                index=self.prefix + name, body=index)
        except elasticsearch.exceptions.ConnectionError as e:
            raise store.StoreConnectionError(
                f'Could not connect to index store {self.hosts}', self.hosts)
        except elasticsearch.RequestError as e:
            if e.error == 'resource_already_exists_exception':
                raise store.ExistingIndex(name)
            raise

    async def _delete_index(self, index, **kwargs):
        if await self._exists(index=index):
            await self.connection.indices.delete(self.prefix + index)
        else:
            logging.error('index {} does not exist'.format(index))

    def delete_index(self, index, **kwargs):
        delete_index_loop = asyncio.get_event_loop()
        delete_index_loop.run_until_complete(self._delete_index(index=index, **kwargs))

    async def _get_dims(self, index):
        index_dims = await asyncio.create_task(self._get_properties(index=index))
        return index_dims['dims']

    async def _get_properties(self, index: str, level: store.IndexLevelEnum = None):
        """Returns the list of feature level properties of a Naiad index"""
        if index not in self._properties:
            try:
                resp = await self.connection.indices.get_mapping(
                    index=[self.prefix + index]
                )
                self._properties[index] = resp[self.prefix + index]['mappings']
            except elasticsearch.exceptions.NotFoundError:
                raise store.IndexNotFound("Index {} not existing".format(index))

        props = self._properties[index]

        if level is None:
            return props['_meta']

        return {
            key: props['properties'][key]['type']
            for key in props['_meta']['{}_properties'.format(level.value)]}

    def properties(self, index: str, level: store.IndexLevelEnum = None):
        """Returns the list of feature level properties of a Naiad index"""
        properties_loop = asyncio.get_event_loop()
        return properties_loop.run_until_complete(self._get_properties(index=index, level=level))

    def granule_properties(self, index):
        """returns the list of granule properties of a Naiad index"""
        return self.properties(index, store.IndexLevelEnum.granule)

    def segment_properties(self, index):
        """returns the list of tile properties of a Naiad index"""
        return self.properties(index, store.IndexLevelEnum.segment)

    async def _register(
        self,
        index: str,
        granules: T.Union[T.List[FeatureIndexRecord], FeatureIndexRecord],
        **kwargs
    ):
        """Register granules and its tiles in bulk mode

        Args:
            index: dataset index where to register the granules
            granules: a list of granules (as FeatureIndexRecord objects) or a dictionary
              where the keys are the granules and the children their
              respective tiles.
        """
        logging.debug("register")

        if not isinstance(granules, list):
            granules = [granules]

        # fix shapes (ensure they are counterclockwise oriented or indexing in
        # ES7 will fail
        def fix_polygon(geom):
            if isinstance(geom, shapely.geometry.MultiPolygon):
                poly = shapely.ops.unary_union(
                    [fix_polygon(_) for _ in geom.geoms]
                )
                poly = ES7Store.split_over_greenwich(poly)
                return poly
            elif isinstance(geom, shapely.geometry.Polygon):
                return shapely.geometry.polygon.orient(geom)
            else:
                return geom

        for g in granules:
            g.geometry = fix_polygon(g.geometry)
            #g.geometry = g.geometry.geoms[1]
            #print(g.geometry.wkt)
            #g.geometry = wkt.loads(wkt.dumps(g.geometry, rounding_precision=3))

        records = []
        for granule in granules:
            tiles = granule.segments

            # remove granule field as it is used in _id already
            geojson = granule.geojson()
            del geojson['granule']

            # add document type
            geojson['level'] = store.IndexLevelEnum.granule.value

            # expand properties
            properties = geojson.pop('properties')
            if properties is not None:
                for k, v in properties.items():
                    geojson[k] = v

            # record to be sent to ES
            rec = {"_index": self.prefix + index,
                   "_id": granule.granule,
                   "_source": json.dumps(geojson)
                   }
            records.append(rec)

            if tiles is None:
                continue

            for tile in tiles:

                # orient geometry ccw
                tile.geometry = fix_polygon(tile.geometry)

                geojson = tile.geojson()

                # add parent relationship with granule
                geojson['level'] = store.IndexLevelEnum.segment.value
                geojson.update({
                    'granule': granule.granule,
                    'feature': granule.feature,
                })

                # expand properties and slices
                properties = geojson.pop('properties')
                slices = geojson.pop('slices')

                if properties is not None:
                    for k, v in properties.items():
                        geojson[k] = v
                if slices is not None:
                    for k, v in slices.items():
                        geojson[k + '_min'] = v[0]
                        geojson[k + '_max'] = v[1]

                rec = {"_index": self.prefix + index,
                       "_id": tile.hash(granule.granule),
                       "_source": geojson
                       }
                records.append(rec)

        try:
            res = await elasticsearch.helpers.async_bulk(
                self.connection,
                actions=records,
                chunk_size=1000,
                max_retries=3,
                yield_ok=False,
                request_timeout=3600.)
            errors = res[1]
        except elasticsearch.helpers.BulkIndexError as e:
            print(e)
            raise

        if len(errors) > 0:
            for err in errors:
                logging.error(f'Failed registrations: {err}')
            raise store.FailedIngestion(
                f'{len(errors)} failed registrations in bulk mode')

    def register(
        self,
        index: str,
        granules: T.Union[T.List[FeatureIndexRecord], FeatureIndexRecord],
        **kwargs
    ):
        """Register granules and its tiles in bulk mode

        Args:
            index: dataset index where to register the granules
            granules: a list of granules (as FeatureIndexRecord objects) or a dictionary
              where the keys are the granules and the children their
              respective tiles.
        """
        register_loop = asyncio.get_event_loop()
        register_loop.run_until_complete(
            self._register(
                index=index,
                granules=granules,
                **kwargs
            )
        )

    async def _register_from_files(
        self,
        name,
        files,
        granule_only=False,
        bulk=True,
        fail_silently=False,
        optimize=True,
        **kwargs
    ):
        if optimize and bulk:
            logging.debug('start ES7 bulk optimization')
            await asyncio.create_task(self._start_bulk_optimization(name))

        if isinstance(files, (Path, str)):
            files = [files]
        if not isinstance(files, list):
            raise ValueError(
                f'The list of files is not of type list but {type(files)}')
        if len(files) == 0:
            logging.warning('No files to register')
            return []

        # evaluate chunk size for bulk ingestion
        if bulk:
            chunksize = self.estimate_bulk_chunk(files, granule_only)
            logging.info("Estimated chunk size: %d" % chunksize)

        records = []
        errors = []
        for i, f in enumerate(files):
            if not Path(f).exists():
                raise Exception("Can not read file : {}".format(f))
            if bulk:
                # concatenate multiple files first, register later
                records.append(read_csv(f, granule_only=granule_only))
            else:
                # register file by file
                try:
                    await asyncio.create_task(self._register(name, read_csv(f, granule_only=granule_only)))

                except Exception as e:
                    self._process_exception(e, errors, fail_silently)

        if bulk:
            try:
                await asyncio.create_task(self._register(name, records))
            except Exception as e:
                self._process_exception(e, errors, fail_silently)

        # optimization of indexing: reactivate previous index settings
        if optimize and bulk:
            logging.debug('stop ES7 bulk optimization')
            await asyncio.create_task(self._end_bulk_optimization(name))

        return errors

    def register_from_files(self, name, files, bulk=True, optimize=True, **kwargs):
        # optimization of indexing: deactivate temporarily some index settings
        register_from_files_loop = asyncio.get_event_loop()
        return register_from_files_loop.run_until_complete(
            self._register_from_files(
                name=name,
                files=files,
                bulk=bulk,
                optimize=optimize,
                **kwargs
            )
        )

    def crossover_with_granules(
        self,
        granules: T.List[FeatureIndexRecord],
        crossed_products: T.Union[str, T.List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        time_window: timedelta = timedelta(hours=2),
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        return_slices: bool = False,
        fail_silently: bool = False,
        **kwargs
    ) -> T.List[Crossover]:
        crossover_with_granules_loop = asyncio.get_event_loop()
        return crossover_with_granules_loop.run_until_complete(
            self._crossover_with_granules(
                granules=granules,
                crossed_products=crossed_products,
                area=area,
                time_window=time_window,
                granule_constraints=granule_constraints,
                segment_constraints=segment_constraints,
                return_slices=return_slices,
                fail_silently=fail_silently,
                **kwargs
            )
        )

    async def _crossover_with_granules(
        self,
        granules: T.List[FeatureIndexRecord],
        crossed_products: T.Union[str, T.List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        time_window: timedelta = timedelta(hours=2),
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        return_slices: bool = False,
        fail_silently: bool = False,
        **kwargs
    ) -> T.List[Crossover]:
        if len(granules) == 0:
            return []

        # build a gross multisearch query to get the crossovers with each
        # reference granule
        ref_granules_area = [
            area.intersection(granule.geometry) for granule in granules]
        ref_granules_start = [
            granule.start - time_window for granule in granules]
        ref_granules_end = [
            granule.end + time_window for granule in granules]

        coresult = await asyncio.create_task(
            self._multisearch(
                crossed_products,
                area=ref_granules_area,
                start=ref_granules_start,
                end=ref_granules_end,
                granule_constraints=granule_constraints,
                segment_constraints=segment_constraints,
                return_slices=return_slices,
                fail_silently=fail_silently
            )
        )

        # keep only the reference granules for which there is an intersection
        # with another index
        # @TODO also checking the match is a feature: this case should not
        #  occur => to be investigated
        matched_granules_ref = []
        for i, crossovers in enumerate(coresult):
            if len(crossovers) > 0:
                matched_granules_ref.extend(
                    [Crossover(granules[i], _) for _ in crossovers
                     if type(_) != SegmentIndexRecord]
                )

        # @TODO should also go through fine search if possible to remove bad
        #  temporal crossovers
        if not return_slices:
            return matched_granules_ref

        # refine search => get segments of reference intersecting the found
        # crossovers. This allows finer temporal matching.
        co_granules_area = [
            _.match.segments[0].geometry for _ in matched_granules_ref]
        co_granules_start = [
            _.match.segments[0].start - time_window
            for _ in matched_granules_ref]
        co_granules_end = [
            _.match.segments[0].end + time_window for _ in matched_granules_ref]
        ref_granules = [_.reference.granule for _ in matched_granules_ref]

        refresult = asyncio.create_task(
            self._multisearch(
                granules[0].dataset_id,
                area=co_granules_area,
                # start=co_granules_start,
                # end=co_granules_end,
                granules=ref_granules,
                return_slices=return_slices,
                fail_silently=fail_silently,
                term_segment=True
            )
        )

        # check sanity of result and rematch
        crossovers = []
        for i, co in enumerate(refresult):
            if len(co) == 0:
                logging.debug('Skipped false crossover')
                continue

            if len(co) != 1:
                raise store.CrossoverError(
                    'Zero or more than one match for the reference dataset '
                    'in a crossover. There should be one.'
                )

            ref = copy.deepcopy(matched_granules_ref[i])
            ref.reference.segments = [refresult[i][0]]
            crossovers.append(ref)

        return crossovers

    def crossover_search(
        self,
        reference: str,
        crossed_index: T.Union[str, T.List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        start: datetime = datetime(1950, 1, 1),
        end: datetime = datetime.now(),
        time_window=timedelta(hours=2),
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        return_slices: bool = False,
        fail_silently: bool = False,
        **kwargs
    ) -> T.List[Crossover]:
        crossover_search_loop = asyncio.get_event_loop()
        return crossover_search_loop.run_until_complete(
            self._crossover_search(
                reference=reference,
                crossed_index=crossed_index,
                area=area,
                start=start,
                end=end,
                time_window=time_window,
                granule_constraints=granule_constraints,
                segment_constraints=segment_constraints,
                return_slices=return_slices,
                fail_silently=fail_silently,
                **kwargs
            )
        )

    async def _crossover_search(
        self,
        reference: str,
        crossed_index: T.Union[str, T.List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        start: datetime = datetime(1950, 1, 1),
        end: datetime = datetime.now(),
        time_window=timedelta(hours=2),
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        return_slices: bool = False,
        fail_silently: bool = False,
        **kwargs
    ) -> T.List[Crossover]:
        logging.debug(f'Cross {reference} with {crossed_index}')
        logging.debug(f'  Area: {area}')
        logging.debug(f'  Start date: {start}')
        logging.debug(f'  End date: {end}')

        # search first granules for reference product intersecting the search
        # area and time frame
        total, result = await asyncio.create_task(
            self._search(
                reference,
                area=area,
                start=start,
                end=end,
                granule_constraints=granule_constraints,
                segment_constraints=segment_constraints,
                return_slices=False,
                fail_silently=fail_silently)
        )

        granules_ref = result[reference]

        # then search intersections with the granules from (an)other dataset(s)
        # (crossovers)
        subres = await asyncio.create_task(
            self._crossover_with_granules(
                granules_ref,
                crossed_index,
                area=area,
                time_window=time_window,
                granule_constraints=granule_constraints,
                segment_constraints=segment_constraints,
                return_slices=return_slices,
                fail_silently=fail_silently,
                **kwargs
            )
        )

        return subres

    def search(
        self,
        indexes: T.Union[str, T.List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        start: datetime = datetime(1950, 1, 1),
        end: datetime = datetime.now(),
        page: int = 0,
        count: int = 1000,
        granules: T.Union[str, T.List[str]] = None,
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        excluded_granules: T.List[str] = None,
        min_intersection: float = 0.,
        return_slices: bool = False,
        return_intersection: bool = False,
        merge_segments: bool = True,
        fail_silently: bool = False,
        **kwargs
    ):
        search_loop = asyncio.get_event_loop()
        return search_loop.run_until_complete(
            self._search(
                indexes=indexes,
                area=area,
                start=start,
                end=end,
                page=page,
                count=count,
                granules=granules,
                granule_constraints=granule_constraints,
                segment_constraints=segment_constraints,
                excluded_granules=excluded_granules,
                min_intersection=min_intersection,
                return_slices=return_slices,
                return_intersection=return_intersection,
                merge_segments=merge_segments,
                fail_silently=fail_silently,
                **kwargs
            )
        )

    async def _search(
        self,
        indexes: T.Union[str, T.List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        start: datetime = datetime(1950, 1, 1),
        end: datetime = datetime.now(),
        page: int = 0,
        count: int = 1000,
        granules: T.Union[str, T.List[str]] = None,
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        excluded_granules: T.List[str] = None,
        min_intersection: float = 0.,
        return_slices: bool = False,
        return_intersection: bool = False,
        merge_segments: bool = True,
        fail_silently: bool = False,
        **kwargs
    ) -> T.Tuple[int, T.Dict[str, T.List[SegmentIndexRecord]]]:
        if isinstance(indexes, str):
            indexes = [indexes]

        if return_slices:
            total, result = await asyncio.create_task(
                self._search_by_segments(
                    indexes, area=area, start=start, end=end,
                    granules=granules,
                    granule_constraints=granule_constraints,
                    segment_constraints=segment_constraints,
                    excluded_granules=excluded_granules,
                    merge_segments=merge_segments,
                    fail_silently=fail_silently
                )
            )
        else:
            # return granules
            total, result = await asyncio.create_task(
                self._search_granule(
                    indexes, area=area, start=start, end=end, page=page, count=count,
                    granules=granules,
                    granule_constraints=granule_constraints,
                    segment_constraints=segment_constraints,
                    excluded_granules=excluded_granules,
                    fail_silently=fail_silently
                )
            )

        granules = result

        # reduces list of granules wrt to intersection area and calculate
        # intersections
        if return_intersection or min_intersection > 0.:
            granules = self.filter_intersection(
                granules, area,
                add_intersection=return_intersection,
                min_intersection=min_intersection)

        # sort result by index
        def sort_granules(granules):
            return {
                index: [_ for _ in granules if _.dataset_id == index]
                for index in indexes}

        return total, sort_granules(granules)

    async def _search_granule(
        self,
        index: T.Union[str, T.List[str]],
        return_errors: bool = False,
        **kwargs
    ):
        """Return the granules within area and time frame.

        Uses granule index level only.
        """
        errors = []

        if type(index) is not list:
            index = [index]

        # build query for granules
        query = self._format_search_query(aggregated_search=False, **kwargs)

        # search granules
        logging.debug(query)
        resp = await self.connection.search(
            index=",".join([self.prefix + _ for _ in index]),
            body=query,
            size=kwargs['count'],
            from_=kwargs['page'],
        )
        result = []
        total = 0
        if resp and 'hits' in resp.keys() and 'hits' in resp['hits'].keys():
            result = [
                await asyncio.create_task(self._decode_hit_result(hit_result)) for hit_result in resp['hits']['hits']
            ]
            if 'total' in resp['hits'].keys() and 'value' in resp['hits']['total'].keys():
                total = resp['hits']['total']['value']

        if return_errors:
            return total, result, errors

        return total, result

    @staticmethod
    def _merge_segments(granule):
        """Aggregates the tiles of a granule and return the disjoint subsets"""
        tiles = granule.segments

        # arrange list of slices per dimension and merge slices
        slices = {dim: [] for dim in tiles[0].slices}
        for tile in tiles:
            for dim, sl in tile.slices.items():
                slices[dim].extend(np.r_[sl])

        # merge slices
        for dim, sl in slices.items():
            slices[dim] = list(set(sl))
            slices[dim].sort()

        # find disjoint slices in each slicing dimension
        disj_slices = {dim: [] for dim in tiles[0].slices}
        for dim, indices in slices.items():
            for k, g in groupby(enumerate(indices), lambda x: x[0] - x[1]):
                group = list(map(itemgetter(1), g))
                disj_slices[dim].append(slice(group[0], group[-1] + 1))

        # group disjoint tiles
        def _indice_group(dim, sli):
            for i in range(len(disj_slices[dim])):
                if sli.start >= disj_slices[dim][i].start and \
                        sli.stop <= disj_slices[dim][i].stop:
                    return i

        # relate the tiles to each group of disjoint tiles
        segments = {}
        for t in tiles:
            group = str({
                d: _indice_group(d, sl) for d, sl in t.slices.items()})
            if group not in segments:
                segments[group] = []
            segments[group].append(t)

        # merge the properties of the tiles in the same group
        granule.segments = []
        for group, tiles in segments.items():
            tile = tiles[0]
            # group slices
            tile.slices = {d: disj_slices[d][v] for d, v in eval(group).items()}
            tile.start = np.array([t.start for t in tiles]).min()
            tile.end = np.array([t.start for t in tiles]).max()
            tile.geometry = shapely.ops.unary_union([t.geometry for t in tiles])

            # @TODO merge properties
            granule.segments.append(tile)

        return granule

    async def _search_by_segments(
        self,
        indexes: T.Union[str, T.List[str]],
        dims: T.List[str] = None,
        merge_segments: bool = True,
        fail_silently: bool = False,
        **kwargs
    ) -> T.Tuple[int, T.List[SegmentIndexRecord]]:
        """Return the granules within an area and time frame, with the
        coordinate slices of their segments intersecting the search
        criteria. More precise than the gross `:func:_search_granule`
        search method.

        Performs a single query to the index's segment level only.

        Args:
            merge_segments: merge the adjacent segments before returning the
               granule (otherwise all found segments are returned in the
               `segments` attribute of each granule.
        """
        if dims is None:
            index = indexes[0]
            dims = await asyncio.create_task(self._get_dims(index=index))

        # build query for the granule and segment documents
        query = self._format_search_query(
            aggregated_search=False, dims=dims, return_slices=True, **kwargs)

        formatted_indexes = ",".join([self.prefix + _ for _ in indexes])

        resp = elasticsearch.helpers.async_scan(
            self.connection,
            index=formatted_indexes,
            query=query,
        )
        result = []
        async for doc in resp:
            decoded_result = await asyncio.create_task(self._decode_hit_result(doc))
            result.append(decoded_result)

        # separate granules and their children segments documents and build a
        # parent (granule) / children (segment) dict
        granules = {
            _.granule: _ for _ in result
            if _.slices is None or len(_.slices) == 0}
        for rec in result:
            if rec.slices is None or len(rec.slices) == 0:
                continue
            if granules[rec.granule].segments is None:
                granules[rec.granule].segments = []
            granules[rec.granule].segments.append(rec)

        granules = list(granules.values())

        # merge segments
        if merge_segments:
            granules = [self._merge_segments(_) for _ in granules]

        # sort by chronological order
        granules.sort(key=attrgetter("start"))

        return len(result), granules

    async def __search_by_segments2(
        self,
        indexes: T.Union[str, T.List[str]],
        return_errors: bool = False,
        dims: T.List[str] = None,
        fill_granule: bool = True,
        return_disjoint: bool = False,
        fail_silently: bool = False,
        **kwargs
    ):
        errors = []
        if dims is None:
            index = indexes[0]
            dims = await asyncio.create_task(self._get_dims(index=index))

        # build search query for tiles
        tquery = self._format_search_query(aggregated_search=True, dims=dims, **kwargs)

        # search both granule and tile documents
        # scroll mode of ES can not be used with aggregation
        # use normal search
        if fill_granule:
            # build query for granules
            gquery = self._format_search_query(aggregated_search=False, **kwargs)
            msearch_res = await asyncio.create_task(
                self.connection.msearch(
                    body=[{}, gquery, {}, tquery],
                    index=[self.prefix + _ for _ in indexes],
                    request_timeout=3600
                )
            )
            resp = msearch_res['responses']
            granules = resp[0]
            subsets = resp[1]

        else:
            subsets = await asyncio.create_task(
                self.connection.search(
                    body=tquery,
                    index=[self.prefix + _ for _ in indexes],
                )
            )

        # missing results : query shall be tuned to return more buckets
        if len(subsets["aggregations"]["result"]["buckets"]) == MAX_BUCKETS:
            msg = 'some results will be missing as they were ignored by ES  ' \
                  'aggregation query. Tune the query to return more results'
            logging.error(msg)
            if not fail_silently:
                raise store.SearchError(msg)
            if return_errors:
                errors.append((INCOMPLETE, msg))

        result = [
            self._decode_agg_result(_, dims=dims)
            for _ in subsets['aggregations']['result']['buckets']
        ]

        # fill in granule information
        if fill_granule:
            ginfo = dict()
            for hit in granules['hits']['hits']:
                key = hit['_id']
                value = await asyncio.create_task(self._decode_hit_result(hit))
                ginfo[key] = value
            for subset in result:
                if subset.granule not in ginfo:
                    msg = "granule {} not returned in granule level " \
                          "search".format(subset.granule)
                    raise store.SearchError(msg)

                subset.start = ginfo[subset.granule].start
                subset.end = ginfo[subset.granule].end
                subset.geometry = ginfo[subset.granule].geometry

        if return_errors:
            return result, errors

        return result

    def _search_by_segments2(
        self,
        indexes: T.Union[str, T.List[str]],
        return_errors: bool = False,
        dims: T.List[str] = None,
        fill_granule: bool = True,
        return_disjoint: bool = False,
        fail_silently: bool = False,
        **kwargs
    ):
        """Return the granules within an area and time frame, with the
        coordinate slices corresponding to the granule segments matching the
        search criteria. More precise than the gross `:func:_search_granule`
        search method.

        Performs a single query to the index's segment level only.
        """
        search_by_segments2_loop = asyncio.get_event_loop()
        return search_by_segments2_loop.run_until_complete(
            self.__search_by_segments2(
                indexes=indexes,
                return_errors=return_errors,
                dims=dims,
                fill_granule=fill_granule,
                return_disjoint=return_disjoint,
                fail_silently=fail_silently,
                **kwargs
            )
        )

    @staticmethod
    def _format_search_query(
        area=None,
        start=datetime(1900, 1, 1),
        end=datetime(3000, 1, 1),
        page: int = 0,
        count: int = 1000,
        granules: T.Union[T.List[str], str] = None,
        excluded_granules: T.Union[T.List[str], str] = None,
        granule_constraints=None,
        segment_constraints=None,
        return_disjoint=False,
        return_slices: bool = False,
        aggregated_search=False,
        dims: T.List[str] = None,
        fail_silently: bool = False,
        term_segment=False
    ):
        """
        Args:
            return_slices: return the offsets of the granule
                matching area, start and/or end criteria (search in tile
                level index which must be existing)
            aggregated_search: performs a bucket aggregated search
            constraints (list, optional): must be expressed as tuples
                (property, operator, value) where operator is either 'eq',
                'lt', 'le', 'gt', 'ge'

        @TODO: segment_constraints
        """
        terms = []
        if area is not None:
            # breakdown the area over meridian 0 into a
            # multipolygon. This is to avoid an intersection bug
            # in ElasticSearch
            #esarea = GeoShape.split_over_greenwich(area)
            #esarea = esarea.simplify(0.1, preserve_topology=True)

            #geojson_area = GeoShape.geojson(esarea)
            geojson_area = shapely.geometry.mapping(area)

            spatialquery = {
                "geo_shape": {
                    "geometry": {
                        "relation": "intersects",
                        "shape": geojson_area
                    }
                }
            }
            terms.append(spatialquery)

        # constraint on properties
        if granule_constraints is not None:
            for cst in granule_constraints:
                prop, oper, value = cst
                if oper == 'eq':
                    terms.append({"term": {prop: value}})
                elif oper in ["gt", "ge", "lt", "le"]:
                    terms.append({"range": {prop: {oper: value}}})

        if granules is not None:
            if not isinstance(granules, list):
                granules = [granules]

            if not term_segment:
                terms.append({"terms": {"_id": granules}})
            else:
                terms.append({"terms": {"granule": granules}})

        query = {
            "query": {
                "bool": {
                    "must": terms
                }
            },
        }

        if start is not None and end is not None:

            temporalquery = [
                {
                    "range": {
                        "time_coverage_start": {
                            "gte": start.strftime("%Y-%m-%dT%H:%M:%S"),
                            "lte": end.strftime("%Y-%m-%dT%H:%M:%S")
                        }
                    }
                },
                {
                    "range": {
                        "time_coverage_end": {
                            "gte": start.strftime("%Y-%m-%dT%H:%M:%S"),
                            "lte": end.strftime("%Y-%m-%dT%H:%M:%S")
                        }
                    }
                },
                {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "time_coverage_start": {
                                        "to": start.strftime(
                                            "%Y-%m-%dT%H:%M:%S"),
                                    }
                                }
                            },
                            {
                                "range": {
                                    "time_coverage_end": {
                                        "from": end.strftime(
                                            "%Y-%m-%dT%H:%M:%S"),
                                    }
                                }
                            }
                        ]
                    }
                }
            ]

            query['query']['bool']['should'] = temporalquery
            query['query']['bool']['minimum_should_match'] = 1

        # granule documents only
        if not return_slices:
            if not aggregated_search:
                query['query']['bool']['filter'] = [
                    {"term": {"level": 'granule'}}
                ]
            else:
                query['query']['bool']['filter'] = [
                    {"term": {"level": 'tile'}}
                ]

        if excluded_granules is not None:
            if not isinstance(excluded_granules, list):
                excluded_granules = [excluded_granules]
            query['query']['bool']['must_not'] = [
                {"terms": {"_id": excluded_granules}},
                {"terms": {"granule": excluded_granules}},
            ]

        if aggregated_search:
            aggregation = {
                "result": {
                    "composite": {
                        # size = maximum number of granules returned
                        "size": MAX_BUCKETS,
                        "sources": [
                            {"granule": {"terms": {"field": "granule"}}},
                            {"dataset_id": {"terms": {"field": "_index"}}}
                        ]
                    },
                    "aggregations": {
                        "time_coverage_start": {
                            "min": {
                                "field": "time_coverage_start"
                            }
                        },
                        "time_coverage_end": {
                            "max": {
                                "field": "time_coverage_end"
                            },
                        }
                    }
                }
            }

            # aggregation over slices
            for d in dims:
                aggregation['result']['aggregations'][d+'_min'] = {
                    "min": {"field": d+'_min'}}
                aggregation['result']['aggregations'][d + '_max'] = {
                    "max": {"field": d + '_max'}}

            query["aggs"] = aggregation
            query["size"] = 0
        else:
            query["size"] = count

        return query

    async def _decode_hit_result(
        self,
        item: T.Dict
    ) -> T.Union[FeatureIndexRecord, SegmentIndexRecord]:
        """Decode an elasticsearch document as FeatureIndexRecord or
        SegmentIndexRecord object.
        """
        shape = shapely.geometry.shape(
            item['_source']["geometry"]
        )
        product = item['_index'].replace(self.prefix, '', 1)

        properties = dict(
            geometry=shape,
            start=datetime.strptime(
                item['_source']["time_coverage_start"],
                "%Y-%m-%dT%H:%M:%S"
            ),
            end=datetime.strptime(
                item['_source']["time_coverage_end"],
                "%Y-%m-%dT%H:%M:%S"
            ),
            feature=item['_source']["feature"].strip(),
            dataset_id=product,
            properties=item['_source'].get('properties', None)
        )

        if item['_source']['level'] == 'granule':
            return FeatureIndexRecord(
                granule=item['_id'], slices = None, **properties)

        else:
            dims = await asyncio.create_task(self._get_dims(index=product))
            return SegmentIndexRecord(
                granule=item['_source']['granule'],
                slices=self._decode_slices(item['_source'], dims),
                **properties)

    @staticmethod
    def _decode_slices(item: T.Dict, dims: T.List[str]) -> T.Dict[str, slice]:
        """decode slices from an item in Elasticsearch response"""
        slices = {}
        for d in dims:
            try:
                start = int(item[d + '_min']["value"])
                stop = int(item[d + '_max']["value"])
            except TypeError:
                start = item[d + '_min']
                stop = item[d + '_max']
            slices[d] = slice(start, stop)

        return slices

    @classmethod
    def _decode_agg_result(cls, result, dims):
        """Decode the result of a bucket query."""
        # decode slices
        slices = cls._decode_slices(result, dims)

        granule, dataset = result["key"].values()

        return FeatureIndexRecord(
            granule,
            dataset_id=dataset.replace(cls.prefix, '', 1),
            feature='',
            start=None,
            end=None,
            segments=[SegmentIndexRecord(
                start=dateutil.parser.parse(
                    result["time_coverage_start"]["value_as_string"]),
                end=dateutil.parser.parse(
                    result["time_coverage_end"]["value_as_string"]),
                slices=slices
            )],
            geometry=None,
            slices=slices
            )

    async def _granule_info(self, index, granule, return_errors=False, fail_silently=False):
        """Returns the description of a granule

        Args:

        """
        errors = []

        try:
            res = await self.connection.get(
                index=self.prefix + index, id=granule, doc_type='_doc')
        except elasticsearch.NotFoundError:
            msg = 'granule {} not found'.format(granule)
            logging.error(msg)
            if not fail_silently:
                raise store.SearchError(msg)
            if return_errors:
                errors.append((NOT_FOUND, msg))
                return [], errors
            return []

        return await asyncio.create_task(self._decode_hit_result(res))

    def granule_info(self, index, granule, return_errors=False, fail_silently=False):
        """Returns the description of a granule

        Args:

        """
        granule_info_loop = asyncio.get_event_loop()
        return granule_info_loop.run_until_complete(
            self._granule_info(
                index=index,
                granule=granule,
                return_errors=return_errors,
                fail_silently=fail_silently,
            )
        )

    async def _msearch(
        self,
        index,
        queries,
        max_requests_per_call=500,
    ):
        # queries must be split or ES will fail (limit can be changed in ES5+)
        iter = 0
        responses = []
        while iter * max_requests_per_call < len(queries):

            # build search
            query_list = ""

            s0 = iter * max_requests_per_call
            s1 = min((iter + 1) * max_requests_per_call, len(queries))
            for search in queries[s0:s1]:
                query_list += json.dumps({})
                query_list += "\n"
                query_list += json.dumps(search)
                query_list += "\n"

            # multi query to ES
            try:
                res = await asyncio.create_task(
                    self.connection.msearch(
                        body=query_list,
                        index=self.prefix + index,
                        request_timeout=3600
                    )
                )
                responses.extend(res['responses'])
            except Exception:
                raise
            finally:
                for r in responses:
                    if 'error' not in r:
                        continue
                    if r['error']['type'] == 'index_not_found_exception':
                        raise store.IndexNotFound(index)

            iter += 1

        assert len(responses) == len(queries)

        return responses

    def _group(
        self,
        records: T.List[T.Union[FeatureIndexRecord, SegmentIndexRecord]],
        merge_segments: bool = False
    ) -> T.List[FeatureIndexRecord]:
        """Organise a list of FeatureIndexRecord and SegmentIndexRecord as
        an ordered list of FeatureIndexRecord which segments are the related
        SegmentIndexRecord objects in the initial list.
        """
        granules = {
            _.granule: _ for _ in records if isinstance(_, FeatureIndexRecord)}
        for rec in records:
            if isinstance(rec, FeatureIndexRecord):
                continue
            if rec.granule not in granules:
                # @TODO should not happen!
                logging.warning(f'{rec.granule} was not found in the list of '
                                f'granules. This should not happen. Skipping.')
                # granules[rec.granule] = FeatureIndexRecord(
                #     granule=rec.granule,
                #     feature=rec.feature,
                #     segments=[rec]
                # )
                continue
            if granules[rec.granule].segments is None:
                granules[rec.granule].segments = []
            granules[rec.granule].segments.append(rec)
        granules = list(granules.values())

        # aggregate segments
        if merge_segments:
            granules = [self._merge_segments(_) for _ in granules]

        # sort by chronological order
        granules.sort(key=attrgetter("start"))

        return granules

    def multisearch(
        self,
        index: str,
        level: store.IndexLevelEnum = store.IndexLevelEnum.granule,
        area: T.Union[Area, T.List[Area]] = shapely.geometry.box(-180., -90., 180., 90.),
        start: T.Union[datetime, T.List[datetime]] = datetime(1950, 1, 1),
        end: T.Union[datetime, T.List[datetime]] = datetime.now(),
        granules: T.Union[str, T.List[str]] = None,
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        excluded_granules: T.Union[T.List[str], T.List[T.List[str]]] = None,
        add_intersection:  bool = False,
        min_intersection: float = 0.,
        dims: T.List[str] = None,
        return_slices: bool = False,
        merge_segments: bool = False,
        fail_silently: bool = False,
        **kwargs
    ) -> MSearchResult:
        multisearch_loop = asyncio.get_event_loop()
        return multisearch_loop.run_until_complete(
            self._multisearch(
                index=index,
                level=level,
                area=area,
                start=start,
                end=end,
                granules=granules,
                granule_constraints=granule_constraints,
                segment_constraints=segment_constraints,
                excluded_granules=excluded_granules,
                add_intersection=add_intersection,
                min_intersection=min_intersection,
                dims=dims,
                return_slices=return_slices,
                merge_segments=merge_segments,
                fail_silently=fail_silently,
                **kwargs
            )
        )

    async def _multisearch(
        self,
        index: str,
        level: store.IndexLevelEnum = store.IndexLevelEnum.granule,
        area: T.Union[Area, T.List[Area]] = shapely.geometry.box(-180., -90., 180., 90.),
        start: T.Union[datetime, T.List[datetime]] = datetime(1950, 1, 1),
        end: T.Union[datetime, T.List[datetime]] = datetime.now(),
        granules: T.Union[str, T.List[str]] = None,
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        excluded_granules: T.Union[T.List[str], T.List[T.List[str]]] = None,
        add_intersection:  bool = False,
        min_intersection: float = 0.,
        dims: T.List[str] = None,
        return_slices: bool = False,
        merge_segments: bool = False,
        fail_silently: bool = False,
        **kwargs
    ):
        _granules = await asyncio.create_task(
            self.__multisearch(
                index,
                level=level,
                area=area,
                start=start,
                end=end,
                granules=granules,
                granule_constraints=granule_constraints,
                segment_constraints=segment_constraints,
                excluded_granules=excluded_granules,
                dims=dims,
                return_slices=return_slices,
                merge_segments=merge_segments,
                fail_silently=fail_silently,
                **kwargs
            )
        )

        # filter out unfitted granules wrt to intersection area and calculate
        # intersections
        if not isinstance(area, list):
            area = [area] * 10
        granules = [
            self.filter_intersection(
                _granules[i], area[i], add_intersection=add_intersection,
                min_intersection=min_intersection)
            for i in range(len(_granules))]

        return granules

    async def __multisearch(
        self,
        index: str,
        level: store.IndexLevelEnum = store.IndexLevelEnum.granule,
        area: T.Union[store.Area, T.List[store.Area]] = shapely.geometry.box(-180., -90., 180., 90.),
        start: T.Union[datetime, T.List[datetime]] = datetime(1950, 1, 1),
        end: T.Union[datetime, T.List[datetime]] = datetime.now(),
        granules: T.Union[str, T.List[str]] = None,
        granule_constraints: store.Constraint = None,
        segment_constraints: store.Constraint = None,
        excluded_granules: T.Union[T.List[str], T.List[T.List[str]]] = None,
        dims: T.List[str] = None,
        return_slices=False,
        return_intersection_areas: bool = False,
        merge_segments: bool = True,
        fail_silently: bool = False,
        max_requests_per_call=500,
        **kwargs
    ) -> T.List[T.Union[SegmentIndexRecord, FeatureIndexRecord]]:
        # number of queries to perform
        count = 1
        for _ in [area, start, end, granules, granule_constraints,
                  segment_constraints, excluded_granules]:
            if isinstance(_, list):
                count = len(_)
                break

        # offset dimensions
        if dims is None:
            dims = await asyncio.create_task(self._get_dims(index=index))

        # build the list of queries
        value = lambda arg, item: arg[item] if type(arg) is list else arg

        queries = []
        for i in range(count):
            queries.append(self._format_search_query(
                granules=value(granules, i),
                area=value(area, i),
                start=value(start, i),
                end=value(end, i),
                granule_constraints=value(granule_constraints, i),
                segment_constraints=value(segment_constraints, i),
                excluded_granules=value(excluded_granules, i),
                return_slices=return_slices,
                dims=dims,
                **kwargs))
        logging.debug(f'{len(queries)} queries in multisearch query')

        # perform search
        responses = await asyncio.create_task(
            self._msearch(
                index, queries, max_requests_per_call=max_requests_per_call
            )
        )

        # decode Elasticsearch query result
        results = []
        for i, resp in enumerate(responses):

            # check if errors
            if 'hits' not in resp and 'error' in resp:
                logging.error(resp['error'])
                if not fail_silently:
                    raise store.CrossoverError(resp['error'])

            if level == store.IndexLevelEnum.granule:
                result = []
                for hit in resp['hits']['hits']:
                    decoded_hit = await asyncio.create_task(self._decode_hit_result(hit))
                    result.append(decoded_hit)
                logging.debug(f'   {len(resp)} results in query {i}')
            else:
                raise NotImplementedError

            results.append(result)

        # double check actual intersection of the found record
        output_intersections = []
        for i, intersects in enumerate(results):
            intersection_areas = [
                area[i].intersection(_.geometry) for _ in intersects]

            if return_intersection_areas:
                for j, rec in enumerate(intersects):
                    rec.intersection_area = intersection_areas[j]

            # remove non-intersecting records
            results[i] = [
                _ for j, _ in enumerate(intersects)
                if not intersection_areas[j].is_empty]

            # if mixed result, separate granules and their children segments
            # documents and build a parent (feature) / children (segments)
            # FeatureIndexRecord object
            is_mixed_record = (
                any([isinstance(_, FeatureIndexRecord) for _ in results[i]])
                and any([isinstance(_, SegmentIndexRecord) for _ in results[i]])
            )
            if is_mixed_record:
                features = self._group(results[i], merge_segments)

                # remove features that don't have a segment (out of time
                # window boundaries
                results[i] = [_ for _ in features if _.segments is not None]

        return results

    @staticmethod
    def connection_args(subparsers):
        parser_store = subparsers.add_parser(
            'ES7', help='Elasticsearch 7 index store')

        parser_store.add_argument(
            '--prefix', action='store',
            help=(
                'prefix to prepend to each index name (may be needed in some '
                'organizations due to specific restrictions to Elasticsearch '
                'access)')
        )

        return parser_store

    @staticmethod
    def creation_args(parser_store):
        parser_store.add_argument(
            '--shards', action='store',
            help='number of shards for the granule level index'
        )

        parser_store.add_argument(
            '--replica', action='store',
            help='number of replicas for the indices'
        )

        return parser_store

    @staticmethod
    def store_config():
        return ES7StoreConfig

    @staticmethod
    def store_creation_config():
        return ES7IndexCreationConfig

    @staticmethod
    def registration_args(parser_store):
        """store-specific arguments for the registration of Naiad index data"""
        parser_store.add_argument(
            '--optimize', action='store_true',
            help=(
                'optimization of naiad indexing by deactivating some '
                'Elasticsearch settings. Only when tile level index is used, '
                'will fail if it is not existing.'))

    @staticmethod
    def registration_config():
        return ES7RegistrationConfig

    @staticmethod
    def split_over_greenwich(shape):
        """Split a shape along meridian 0 and returns a multipolygon instead.

        Assume the initial shape is valid and within -180/-90/180/90 limits,
        unwrapped over pole and dateline.

        Workaround to fix Elasticsearch Tesselate issue

        Args:
            shape (Polygon, MultiPolygon): a shapely geometry object.

        Returns:
            MultiPolygon: a shapely multipolygon matching the original shape
            area.

        """
        splittiles = []
        if shape.geom_type == 'MultiPolygon':
            shapes = shape.geoms
        else:
            shapes = [shape]

        for pol in shapes:
            # fix some small polygon issues resulting in a
            # GeometryCollection when intersecting below
            #pol = pol.buffer(0.01)

            if pol.intersects(
                    shapely.geometry.LineString([(0., -90.), (0, 90.)])):

                tilewest = pol.intersection(WORLD_WEST0)
                if tilewest.geom_type == "GeometryCollection":
                    logging.error("Wrong intersection with globe map. This is"
                                  "likely caused by a weird polygon case."
                                  "Contact the developer.")
                    raise Exception("Bad intersection result")

                if not tilewest.is_empty:
                    if tilewest.geom_type != 'MultiPolygon':
                        splittiles.append(tilewest)
                    else:
                        splittiles.extend(tilewest.geoms)

                tileeast = pol.intersection(WORLD_EAST0)
                if tileeast.geom_type == "GeometryCollection":
                    logging.error("Wrong intersection with globe map. This is"
                                  "likely caused by a weird polygon case."
                                  "Contact the developer.")

                if not tileeast.is_empty:
                    if tileeast.geom_type != 'MultiPolygon':
                        splittiles.append(tileeast)
                    else:
                        splittiles.extend(tileeast.geoms)
            else:
                splittiles.append(pol)

        # re-unite all polygons in a multipolygon
        if len(splittiles) > 1:
            return shapely.geometry.MultiPolygon(splittiles)
        else:
            return splittiles[0]
