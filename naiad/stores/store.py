# -*- coding: utf-8 -*-
"""
Interface to Naiad index storage

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import ast
import logging
import os
import random
from abc import abstractmethod
from datetime import datetime, timedelta
from enum import Enum
from pathlib import Path
from typing import List, Dict, Union, Tuple, Any

import shapely.geometry
from naiad_commons.data.geoindexrecord import FeatureIndexRecord, \
    SegmentIndexRecord, Crossover
from pydantic import BaseModel, Extra, validator

from naiad.cli import FormatEnum
from naiad.env import STORES

# type hint aliases
Area = Union[shapely.geometry.Polygon, shapely.geometry.MultiPolygon]
GranuleResult = Dict[str, List[FeatureIndexRecord]]
ErrorResult = List[Tuple[str, str]]
MSearchResult = Union[
    List[List[FeatureIndexRecord]],
    Tuple[
        List[List[FeatureIndexRecord]],
        List[List[Tuple[str, str]]]
    ]
]
Constraint = List[Tuple[str, str, str]]
CrossOverResult = Dict[FeatureIndexRecord, List[FeatureIndexRecord]]
CrossSearchResult = Union[
    CrossOverResult, Tuple[CrossOverResult, ErrorResult]
]


# Exceptions
class IndexNotFound(Exception):
    def __init__(self, index):
        super().__init__(f'{index} not found')


class SearchError(Exception):
    pass


class CrossoverError(Exception):
    pass


class StoreConnectionError(Exception):
    def __init__(self, message, host, *args):
        self.message = message
        self.host = host
        super(StoreConnectionError, self).__init__(message, *args)


class FailedIngestion(Exception):
    def __init__(self, message, unregistered, *args):
        self.message = message
        self.unregistered = unregistered
        super(FailedIngestion, self).__init__(message, *args)


class ExistingIndex(Exception):
    def __init__(self, name, *args):
        message = "Index {} already existing".format(name)
        super(ExistingIndex, self).__init__(message, *args)


class StoreConfig(BaseModel):
    store_type: str = 'ES7'
    host: str = 'http://localhost:9200'
    username: str = None
    password: str = None
    prefix: str = 'naiad_'


class IndexCreationConfig(StoreConfig):
    index: str = None
    dims: List[str] = None


class RegistrationConfig(StoreConfig):
    granule_only: bool = False
    skip_index_check: bool = False
    bulk: bool = False


class OperatorEnum(str, Enum):
    gt = "gt"
    ge = "ge"
    lt = "lt"
    le = "le"


class SearchConfig(BaseModel):
    class Config:
        extra = Extra.ignore
        arbitrary_types_allowed = True

    area: shapely.geometry.Polygon = shapely.geometry.box(
        -180., -90., 180., 90.)
    start: datetime = datetime(1950, 1, 1)
    end: datetime = datetime.now()
    granule_constraints: List[Tuple[str, OperatorEnum, Any]] = None
    subset_constraints: List[Tuple[str, OperatorEnum, Any]] = None
    return_slices: bool = False
    output_format: FormatEnum = FormatEnum.list
    datastore: Path = None
    toolstore: Path = None
    exttool: str = None
    show_each: bool = False
    show_all: bool = False
    add_fullpath: bool = False
    storage_path: Path = None

    @validator('area')
    def check_type(cls, v):
        assert isinstance(v, shapely.geometry.Polygon)
        return v


class CrossSearchConfig(BaseModel):
    class Config:
        extra = Extra.ignore
        arbitrary_types_allowed = True

    cross: str
    versus: str
    area: shapely.geometry.Polygon = shapely.geometry.box(
        -180., -90., 180., 90.)
    start: datetime = datetime(1950, 1, 1)
    end: datetime = datetime.now()
    granule_constraints: List[Tuple[str, OperatorEnum, Any]] = None
    subset_constraints: List[Tuple[str, OperatorEnum, Any]] = None
    return_slices: bool = False
    output_format: FormatEnum = FormatEnum.list
    datastore: Path = None
    toolstore: Path = None
    exttool: str = None
    show_each: bool = False
    show_all: bool = False
    add_fullpath: bool = False
    storage_path: Path = None


class IndexLevelEnum(str, Enum):
    granule = "granule"
    segment = "tile"


class Store(object):
    """
    Base class for Naiad index storage

    Args:
        host: Store URL(s) to connect to. If not provided, Naiad will search
            for ``NAIAD_URL`` environment variable. If it is not set, Naiad will
            use the Store class default value, if any.
    """

    def __init__(
            self,
            host: List[str] = None,
            username: str = None,
            password: str = None,
            verbosity: int = logging.WARNING,
            **kwargs):

        if host is None:
            # use default Naiad store host if defined
            if "NAIAD_URL" in os.environ:
                host = os.environ["NAIAD_URL"]
                if '[' in host:
                    host = ast.literal_eval(host)
            else:
                # use localhost by default
                host = self.default_url()

        if not isinstance(host, list):
            host = [host]
        self.hosts = host

        logging.debug("Index storage hosts : {}".format(host))

        # authentication parameters
        self.login = username
        self.password = password

        # verbosity
        self.set_verbosity(verbosity)

        # connect
        self.connection = self.connect(**kwargs)

        # cache for indexes properties
        self._properties = {}

    def connect(self, **kwargs):
        """Connect to the index store"""
        raise NotImplementedError

    @abstractmethod
    def default_url(self):
        raise ValueError("No default host defined; please provide a host or "
                         "list of hosts.")

    @abstractmethod
    async def indices(self, **kwargs) -> List[str]:
        """Return the list of Naiad indices on the host"""
        raise NotImplementedError

    def properties(self, index: str, level: IndexLevelEnum = None):
        """Returns the list of feature level properties of a Naiad index"""
        raise NotImplementedError

    def dims(self, index):
        """return the slice dimensions of an index"""
        return self.properties(index)['dims']

    @abstractmethod
    def exists(self, index: str, **kwargs) -> bool:
        """
        Check if an index is existing.

        Args:
            index (str): name of the index

        Returns:
            A boolean indicating whether the passed index exists
        """
        raise NotImplementedError

    def set_verbosity(self, level: int = logging.warning):
        """Set verbosity level when talking to the storage"""
        pass

    @abstractmethod
    def create_index(self, index, dims, granule_only=True,
                     granule_properties=None, segment_properties=None, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def delete_index(self, index, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def register(self, name, granules, segments, **kwargs):
        """register multiple granules and/or tiles"""
        raise NotImplementedError

    @abstractmethod
    def granule_properties(self, name) -> Dict:
        """returns the list of feature level properties of a Naiad index"""
        raise NotImplementedError

    @abstractmethod
    def segment_properties(self, name) -> Dict:
        """returns the list of tile properties of a Naiad index"""
        raise NotImplementedError

    @abstractmethod
    def granule_info(self, name: str, granule: str):
        """Returns the description of a granule

        Args:
            name: name of the index
            granule: identifier of the granule
        """
        raise NotImplementedError

    @staticmethod
    def _process_exception(exception, errors, fail_silently=False):
        if not fail_silently:
            logging.error(str(exception))
            raise
        errors.append(exception)

    @staticmethod
    def estimate_bulk_chunk(files: List[Path], granule_only: bool = False):
        """Evaluate chunk size for bulk ingestion. To be overridden per store
        type.
        """
        # optimal chunk size: 5-15 MB
        optimum_chunk_volume = 10 * (1024 ** 2)

        # take a random file
        randfile = files[random.randint(0, len(files) - 1)]

        if granule_only:
            # average size of a granule json size : read first line
            with open(randfile, 'r') as f:
                granule_size = len(f.readline())
            chunksize = int(optimum_chunk_volume / granule_size)
        else:
            # take size of a random tile file
            tile_size = Path(randfile).stat().st_size
            chunksize = int(optimum_chunk_volume / tile_size)

        logging.info("Estimated chunk size: %d" % chunksize)

        return chunksize

    def register_from_files(
            self, name, files: List[Union[str, Path]], granule_only=False,
            bulk=True, fail_silently=False, **kwargs):

        raise NotImplementedError

    def _search_by_segments(
        self,
        indexes: Union[str, List[str]],
        dims: List[str] = None,
        fill_granule: bool = True,
        fail_silently: bool = False,
        **kwargs
    ):
        """Return the granules within an area and time frame, with the
        coordinate slices corresponding to the granule segments matching the
        search criteria. More precise than the gross `:func:_search_granule`
        search method.

        Performs a single query to the index's segment level only.
        """
        raise NotImplementedError

    def _search_granule(
        self,
        index: Union[str, List[str]],
        fail_silently: bool = False,
        **kwargs
    ):
        """Return the granules within area and time frame. Based on the
        global granule footprint and time frame, so may lack precision
        depending on the size of the corresponding features.

        Performs a single query to the index's granule level only.
        """
        raise NotImplementedError

    def search(
        self,
        indexes: Union[str, List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        start: datetime = datetime(1950, 1, 1),
        end: datetime = datetime.now(),
        page: int = 0,
        count: int = 1000,
        granules: Union[str, List[str]] = None,
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        excluded_granules: List[str] = None,
        min_intersection: float = 0.,
        return_slices: bool = False,
        return_intersection: bool = False,
        merge_segments: bool = True,
        fail_silently: bool = False,
        **kwargs
    ) -> Tuple[int, Dict[str, List[SegmentIndexRecord]]]:
        """Search the granules, from one or multiple indexes, intersecting an
        area and time frame of interest.

        Args:
            indexes: dataset index(es) where to search granules from
            area (BaseGeometry): area of interest
            start (datetime): start of selection time frame
            end (datetime): end of selection time frame
            granule_constraints (list, optional): search constraints on the
                granule properties. Must be expressed as tuples (property,
                operator, value) where operator is either 'eq', 'lt', 'le',
                'gt', 'ge'. Ex: [('zensol', 'gt', 90.)]
            return_intersection: calculate the intersection geometry between
                each found item and the intersection area
            min_intersection (float): percentage of minimum intersection with
                the search area (if 0, any intersecting result will be kept)
            merge_segments: merge the adjacent segments before returning the
               granule (otherwise all found segments are returned in the
               `segments` attribute of each granules.
        """
        raise NotImplementedError

    @staticmethod
    def add_store_connection_args(parser):
        """Connection arguments for a CLI"""
        parser.add_argument(
            '-sn', '--store', dest='store_name', action='store',
            help='name of the store as defined in your configuration file')

        subparsers = parser.add_subparsers(dest='store_type')

        # add store specific argument subparsers
        store_parsers = {}
        for store in STORES:
            parser_store = STORES[store].load().connection_args(
                subparsers)
            parser_store.add_argument(
                '-u', '--url', dest='host', action='store',
                help='URL of Naiad index store for the requested index(es).')
            parser_store.add_argument(
                '-l', '--login', action='store', dest='username',
                help='login for secured connection to index store'
            )
            parser_store.add_argument(
                '-p', '--password', action='store',
                help='password for secured connection to index store'
            )
            store_parsers[store] = parser_store

        return store_parsers

    @staticmethod
    def decode_store_connection_args(args, config=None):
        # priority : args > config > class/processing defaults
        vargs = {k: v for k, v in vars(args).items() if v is not None}

        # priority : args > config > class/processing defaults
        if args.store_name is not None:
            if config is None or args.store_name not in config.stores:
                raise ValueError(
                    'configuration for {} store was not found. Check it is '
                    'defined in your configuration file or that you have a '
                    'configuration file')
            def_args = config.stores[args.store_name].dict()
        else:
            def_args = StoreConfig().dict()

        # overrides with command args
        def_args.update(vargs)

        # store specific args
        store_class = STORES[def_args['store_type']].load()

        return store_class.store_config()(**def_args)

    @staticmethod
    def add_creation_args(parser):
        parsers = Store.add_store_connection_args(parser)
        for store, store_parser in parsers.items():
            STORES[store].load().creation_args(store_parser)

    @staticmethod
    def decode_creation_args(args, config=None):
        # priority : args > config > class/processing defaults
        vargs = {k: v for k, v in vars(args).items() if v is not None}

        # connection args
        vargs.update(
            Store.decode_store_connection_args(args, config=config).dict())

        # configuration defaults
        if args.store_name is not None and config is not None and \
                args.store_name in config.stores:
            def_args = config.stores[args.store_name].dict()
        else:
            def_args = IndexCreationConfig().dict()

        # overrides with command args
        def_args.update(vargs)

        # store specific args
        store_class = STORES[def_args['store_type']].load()

        return store_class.store_creation_config()(**def_args)

    @staticmethod
    def add_registration_args(parser):
        parsers = Store.add_store_connection_args(parser)
        for store, store_parser in parsers.items():
            STORES[store].load().registration_args(store_parser)

    @staticmethod
    def decode_registration_args(args, config=None):
        # priority : args > config > class/processing defaults
        vargs = {k: v for k, v in vars(args).items() if v is not None}

        # connection args
        vargs.update(
            Store.decode_store_connection_args(args, config=config).dict())

        # configuration defaults
        if args.store_name is not None and config is not None and \
                args.store_name in config.stores:
            def_args = config.indexes[args.index].registration.dict()
        else:
            def_args = RegistrationConfig().dict()

        # overrides with command args
        def_args.update(vargs)

        # store specific args
        store_class = STORES[def_args['store_type']].load()

        return store_class.registration_config()(**def_args)

    @staticmethod
    def decode_search_args(args, config):
        # priority : args > config > class/processing defaults
        vargs = {k: v for k, v in vars(args).items() if v is not None}

        # type info
        if 'area' in vargs and not isinstance(
                vargs['area'], shapely.geometry.Polygon):
            vargs['area'] = shapely.geometry.box(*vargs['area'])

        # connection args
        vargs.update(
            Store.decode_store_connection_args(args, config=config).dict())

        # configuration defaults
        def_args = SearchConfig().dict()

        # overrides with command args
        def_args.update(vargs)

        # store specific args
        store_class = STORES[def_args['store_type']].load()

        return store_class.search_config()(**def_args)

    @staticmethod
    def search_config():
        return SearchConfig

    @staticmethod
    def intersect(
        granule: FeatureIndexRecord,
        area: Area,
        min_intersection: float,
        add_intersection: True
    ) -> bool:
        """Returns True if a granule intersects an area by more than a given
        ratio of its spatial coverage

        Args:
            granule: granule intersecting the search area
            area: search area the intersection is calculated over
            min_intersection: minimum intersection ratio, in percent
            add_intersection: add the shape of the intersection area in
                granule.subsets attribute
        """
        intersection = granule.geometry.intersection(area)
        if isinstance(
                granule.geometry,
                (shapely.geometry.Polygon, shapely.geometry.MultiPolygon)):
            ratio = intersection.area / granule.geometry.area * 100.
        else:
            ratio = intersection.length / granule.geometry.length * 100.

        # not enough intersection: drop this granule
        if ratio < min_intersection:
            return False

        if add_intersection:
            if granule.segments is not None:
                for s in granule.segments:
                    s.geometry = s.geometry.intersection(area)
            else:
                granule.segments = [SegmentIndexRecord(geometry=intersection)]

        return True

    @staticmethod
    def filter_intersection(
        granules: List[FeatureIndexRecord],
        area: Area,
        add_intersection: bool = False,
        min_intersection: float = 0.
    ):
        """Reduce the list of granules wrt to intersection with the search area
        and return the intersection geometries.

        The intersection ratio (as a percentage) is the fraction of the
        granule that intersects the search area. This allows to discard
        results that have only a minor intersection with the search area.
        """
        # remove empty geometries
        granules = [_ for _ in granules if not _.geometry.is_empty]

        # drop granules with little intersection and/or fill in intersection
        # geometry
        if add_intersection or min_intersection > 0:
            granules = [
                _ for _ in granules if Store.intersect(
                    _,
                    area=area,
                    min_intersection=min_intersection,
                    add_intersection=add_intersection)]

        return granules

    def multisearch(
        self,
        index: str,
        level: IndexLevelEnum = IndexLevelEnum.granule,
        area: Union[Area, List[Area]] = shapely.geometry.box(-180., -90., 180., 90.),
        start: Union[datetime, List[datetime]] = datetime(1950, 1, 1),
        end: Union[datetime, List[datetime]] = datetime.now(),
        granules: Union[str, List[str]] = None,
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        excluded_granules: Union[List[str], List[List[str]]] = None,
        add_intersection:  bool = False,
        min_intersection: float = 0.,
        dims: List[str] = None,
        return_slices: bool = False,
        merge_segments: bool = False,
        fail_silently: bool = False,
        **kwargs
    ) -> MSearchResult:
        """Search granules wrt different criteria"""
        raise NotImplementedError

    def crossover_with_granules(
        self,
        granules: List[FeatureIndexRecord],
        crossed_products: Union[str, List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        time_window: timedelta = timedelta(hours=2),
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        return_slices: bool = False,
        fail_silently: bool = False,
        **kwargs
    ) -> List[Crossover]:
        """get crossovers with the reference granule using the polygon
        intersection with other granules shapes.

        Returns the intersection areas but the files slices can only be
        retrieved with a 2nd pass (precise=True). Response time will be
        however slower.

        This query is less precise in terms of time if precise option is
        not used as only the start/end times of the granules are used for
        time matching. Intersections not timely matched are filtered out
        when using precise=True.

        Args:
            method (str): "granule" or "tile". If "granule", use the
                crossover search implemented by
                :func:`intersections_from_granules`. If "tile", use the
                crossover search implemented by
                :func:`intersections_from_tiles`.
            percent (float, optional): minimum intersection percentage
                with the reference granule. Crossovers whose intersection
                percentage is lower than this threshold are not returned.

            server (optional, naiad.queries.server.Server): the Naiad server on
                which to run the search query. If not provided, Naiad will
                instantiate a default server, using the url provided in
                ``NAIAD_ELASTICSEARCH`` environment variable (if set) or
                localhost (``http://localhost:9200`` which is ElasticSearch
                default address. If running multiple queries is is much more
                efficient to instantiate your Naiad server object out of this
                function and pass it in ``server`` argument.
            precise (bool, optional): precise colocation (ensure strict
                temporal matching within the time window at each cross-over.
                If not set, the temporal matching is made at granule level.
        """
        raise NotImplementedError

    def crossover_search(
        self,
        reference: str,
        crossed_index: Union[str, List[str]],
        area: Area = shapely.geometry.box(-180., -90., 180., 90.),
        start: datetime = datetime(1950, 1, 1),
        end: datetime = datetime.now(),
        time_window=timedelta(hours=2),
        granule_constraints: Constraint = None,
        segment_constraints: Constraint = None,
        return_slices: bool = False,
        fail_silently: bool = False,
        **kwargs
    ) -> List[Crossover]:
        """run the search query

        Args:

            return_slices (bool): if False (default), return the full granule
                intersecting the search area and time frame. If true, search
                further to only return the granule subset matching this
                intersection.
        """
        raise NotImplementedError
