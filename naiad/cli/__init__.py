from enum import Enum
import logging

from dateutil import parser

logger = logging.getLogger()
logger.setLevel(logging.WARNING)


class VerbosityEnum(str, Enum):
    verbose = 'verbose'
    quiet = 'quiet'
    silent = 'silent'


class FormatEnum(str, Enum):
    list = "list"
    csv = "csv"
    detailed = "detailed"
    json = "json"


def coord_list(coords_str):
    return tuple([float(_) for _ in coords_str.split(',')])


def date(date_str):
    return parser.parse(date_str)


def constraints(constr_str):
    return [_.split(' ') for _ in constr_str.split(';')]


def logging_level(verbosity: VerbosityEnum):
    try:
        return {
            VerbosityEnum.verbose: logging.DEBUG,
            VerbosityEnum.quiet: logging.WARNING,
            VerbosityEnum.silent: logging.FATAL
        }[verbosity]
    except KeyError:
        return logging.INFO


def set_verbosity(verbosity: VerbosityEnum):
    """Set the verbosity"""
    logger.setLevel(logging_level(verbosity))

