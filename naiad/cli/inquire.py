"""
Command line tool inquiring dataset index properties.

:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import argparse
import logging
import sys
from enum import Enum
from typing import Union

from colorama import Fore, Style
from colorama import init
from pydantic import BaseModel

import naiad
import naiad.utils.config as cfg
from naiad.stores.store import Store, StoreConfig, IndexNotFound

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class InquireEnum(str, Enum):
    list = "list"
    properties = "properties"
    tile_properties = "tile_properties"


class InquireType(BaseModel):
    command: InquireEnum = InquireEnum.list


class ListCommand(InquireType):
    prefix: str = None


class IndexInquireCommand(InquireType):
    index: str


class InquireCommand(BaseModel):
    inquire_args: Union[IndexInquireCommand, ListCommand]
    store: StoreConfig = StoreConfig()
    verbosity: cfg.VerbosityConfig = cfg.VerbosityConfig()


def add_inquire_args(parser):

    command_group = parser.add_mutually_exclusive_group()

    # A command to list the properties of an index
    command_group.add_argument(
        '--properties', action='store_true',
        help='list the properties stored with a granule in an index.'
    )
    command_group.add_argument(
        '--tile-properties', action='store_true',
        help='list the properties stored with each tile in an index.'
    )
    # A command to list available indices
    command_group.add_argument(
        '--list', action='store_true', help='list all indexes in store')

    parser.add_argument(
        '--prefix', action='store', dest='prefix',
        help='prefix of the indexes to list (use in combination with --list)')
    parser.add_argument(
        '--index', action='store', dest='index',
        help='index to look properties of (use in combination with '
             '--properties or --tile-properties)')


def decode_inquire_args(args):
    vargs = vars(args)
    command = next(iter([_.value for _ in InquireEnum if vargs[_]]), None)
    if command is None:
        command = InquireEnum.list
    vargs.update({'command': command})

    if command == InquireEnum.list:
        return ListCommand(**vargs)
    else:
        return IndexInquireCommand(**vargs)


def parse_args(cli_args):
    parser = argparse.ArgumentParser(
        description='Return information on existing Naiad indexes'
    )

    add_inquire_args(parser)
    cfg.add_verbosity_args(parser)
    cfg.add_config_args(parser)
    Store.add_store_connection_args(parser)

    args = parser.parse_args(cli_args)

    # read defaults from config
    defaults = cfg.decode_config_args(args)

    # build inquire processing args
    process_args = InquireCommand(
        inquire_args=decode_inquire_args(args),
        verbosity=cfg.decode_verbosity_args(args, defaults.verbosity),
        store=Store.decode_store_connection_args(args, defaults),
    )

    return process_args


def main(cli_args=None):
    args = parse_args(cli_args)

    # init color terminal
    init()

    # Set up verbosity option.
    naiad.cli.set_verbosity(args.verbosity.level)

    # instantiate Store object
    store = naiad.connect(
        args.store.store_type, **args.store.dict(), **args.verbosity.dict())

    if args.inquire_args.command == InquireEnum.list:
        indices = store.indices(prefix=args.inquire_args.prefix)
        print("\nAvailable Naiad indices: ")
        for ind in indices:
            print("    " + ind)
        print("\n")

    else:
        try:
            if args.inquire_args.command == InquireEnum.tile_properties:
                properties = store.segment_properties(args.inquire_args.index)
                doctype = "tile"
            else:
                properties = store.granule_properties(args.inquire_args.index)
                doctype = "granule"

            print("\n{} properties for index ".format(doctype) +
                  Style.BRIGHT + Fore.BLUE + "{}".format(
                args.inquire_args.index) + " :\n" + Style.RESET_ALL)
            for k, v in properties.items():
                print("......." +
                      Style.BRIGHT + "{} : ".format(k) + Style.RESET_ALL + v)
            print("\n\n")

        except IndexNotFound:
            print("ERROR: Index " +
                  Fore.RED + "{}".format(args.index) + Style.RESET_ALL +
                  " is not existing."
                  )
            store.close_connection()
            sys.exit(1)

    store.close_connection()
    sys.exit(0)
