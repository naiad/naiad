"""

Command line tool for searching granules

:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from __future__ import print_function

import argparse
import json
import os
import sys

from pydantic import BaseModel

import naiad
import naiad.utils.config as cfg
from naiad.data.georecord import GeoRecord
from naiad.stores.store import (
    Store, StoreConfig, SearchConfig, StoreConnectionError)
from naiad.utils.executor import Executor


class SearchCommand(BaseModel):

    index: str
    show_each: bool = False
    store: StoreConfig = StoreConfig()
    verbosity: cfg.VerbosityConfig = cfg.VerbosityConfig()
    search: SearchConfig = SearchConfig()


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, GeoRecord):
            return obj.geojson()
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


def add_search_args(parser):
    parser.add_argument(
        '--index', type=str, required=True,
        help='identifier of the product index to search in (must be lowercase)'
    )
    parser.add_argument(
        '--area', type=naiad.cli.coord_list,
        help='area of interest, defined as "lonmin,latmin,lonmax,latmax"'
        )
    parser.add_argument(
        '--start', type=naiad.cli.date,
        help='start time of the period'
        )
    parser.add_argument(
        '--end', type=naiad.cli.date,
        help='end time of the period'
        )
    parser.add_argument(
        '--return_offsets', action='store_true',
        help='precise search : ensure strict temporal matching within the time '
             'window at the search. If not set, the temporal matching is made '
             'at granule level. Returns the slices of the granule subset '
             'matching the search criteria.'
         )
    parser.add_argument(
        '--subset-constraints', type=naiad.cli.constraints,
        help='specify some constraints on properties to subset the '
             'granules only on relevant content. Only the subsets '
             'of the granule matching the constraints will be '
             'returned.'
        )
    parser.add_argument(
        '--granule-constraints', type=naiad.cli.constraints,
        help='specify some constraints on properties at granule '
             'level. Only the granules matching the specified '
             'constraints will return a result.'
        )
    parser.add_argument(
        '--full-path', action='store_true',
        help="return the matching granules with their full local "
             "name"
        )
    parser.add_argument(
        '--datastore', type=str,
        help='path to the datastore configuration file defining '
             'the access rules to the granule files. Optional arg '
             'and only applicable if --full-path is set.'
        )
    parser.add_argument(
        '--toolstore', type=str,
        help='path to the toolstore configuration file defining '
             'how to pipe the naiad search result in an external tool. '
             'Onl applicable if --exttool is set.'
        )
    parser.add_argument(
        '--exttool', type=str,
        help="name of the tool, configured in the toolstore "
             "configuration file, into which are piped the returned granules"
        )
    parser.add_argument(
        '--output-format', type=str,
        help="format of the returned result, among: list, detailed, json"
        )
    parser.add_argument(
        '--show-each', action='store_true',
        help="display each found granule's foot print on a separate map"
    )
    parser.add_argument(
        '--show-all', action='store_true',
        help='display all found granule in one map'
    )


def parse_args(cli_args):
    parser = argparse.ArgumentParser(
        description='Search the granules matching a region and period of '
                    'interest'
    )

    add_search_args(parser)
    cfg.add_verbosity_args(parser)
    cfg.add_config_args(parser)
    Store.add_store_connection_args(parser)

    args = parser.parse_args(cli_args)

    # read defaults from config
    defaults = cfg.decode_config_args(args)

    process_args = SearchCommand(
        index=args.index,
        verbosity=cfg.decode_verbosity_args(args, defaults.verbosity),
        search=Store.decode_search_args(args, defaults),
        store=Store.decode_store_connection_args(args, defaults),
    )

    return process_args


def main(cli_args=None):
    args = parse_args(cli_args)

    # Set up verbosity option.
    naiad.cli.set_verbosity(args.verbosity.level)

    try:
        # instantiate Store object
        store = naiad.connect(
            args.store.store_type, **args.store.dict(), **args.verbosity.dict())

        total, granules = store.search(
            args.index, **args.search.dict()
        )

    except StoreConnectionError as e:
        print(e.message)
        sys.exit(1)
    finally:
        store.close_connection()

    errors = []
    if isinstance(granules, tuple):
        granules, errors = granules
    granules = granules[args.index]

    if args.verbosity.level == naiad.cli.VerbosityEnum.verbose:
        print("Got %d results:\n" % len(granules))
        if len(errors) != 0:
            print(errors)

    # pipe result in external tool
    if args.search.exttool is not None:
        toolstore = Executor(args.search.toolstore)
        max_files = toolstore.get_maximum_inputs(args.search.exttool)

        granules = [_.granule for _ in granules]

        # one file at a time
        if max_files == 1:
            for granule in granules:
                command = toolstore.get_command(args.search.exttool, granule)
                os.system(command)

        # several files at a time
        else:
            bucket_start = 0
            while bucket_start < len(granules):
                bucket = granules[
                         bucket_start:min(len(granules) - 1, max_files)]
                command = toolstore.get_command(args.search.exttool, bucket)
                os.system(command)
                bucket_start += max_files

    # print result in stdout
    else:
        if args.search.output_format == naiad.cli.FormatEnum.list:
            for granule in granules:
                print(granule.granule)

        elif args.search.output_format == naiad.cli.FormatEnum.csv:
            for granule in granules:
                print(granule.encode_csv())

        elif args.search.output_format == naiad.cli.FormatEnum.detailed:
            for granule in granules:
                print(granule)

        elif args.search.output_format == naiad.cli.FormatEnum.json:
            print(json.dumps(granules, cls=Encoder, sort_keys=True,
                             indent=4, separators=(',', ': ')))

        # display
        if args.search.show_each:
            GeoRecord.show_all(granules)

        elif args.search.show_all:
            for granule in granules:
                granule.show(clip=args.search['area'])

    sys.exit(0)
