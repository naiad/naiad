#!/usr/bin/env python
"""
Command line tool for the search of crossovers between two indexes
"""
from __future__ import print_function

import argparse
import json
import sys

# from colorama import init
# from colorama import Fore, Back, Style
from pydantic import BaseModel

import naiad.utils.config
from naiad.stores.store import (Store, CrossSearchConfig, StoreConnectionError)


class CrossSearchCommand(BaseModel):
    show_each: bool = False
    store: naiad.stores.store.StoreConfig
    verbosity: naiad.utils.config.VerbosityConfig
    crosssearch: CrossSearchConfig


def add_crosssearch_args(parser):
    parser.add_argument('--cross', type=str, required=True,
                        help='name of the product (must be lowercase)')
    parser.add_argument(
        '--versus', type=str, required=True,
        help='name of the products for which to search crossovers with'
        )
    parser.add_argument(
        '--area', type=naiad.cli.coord_list,
        help='area of interest, defined as "lonmin,latmin,lonmax,latmax"'
        )
    parser.add_argument(
        '--start', type=naiad.cli.date,
        help='start time of the period, expressed as YYYYMMDDTHHMMSS'
        )
    parser.add_argument(
        '--end', type=naiad.cli.date,
        help='end time of the period, expressed as YYYYMMDDTHHMMSS'
        )
    parser.add_argument(
        '--crossover-percent', type=float, default=0,
        help='minimum intersection percentage with the reference '
             'granule. Crossovers whose intersection percentage is'
             ' lower than this threshold are not returned.'
        )
    parser.add_argument(
        '--method', type=str, default="granule",
        help='search method : granule (default) or tile'
        )
    parser.add_argument(
        '--time-window', type=int, default=120,
        help='maximum time window, in minutes, for the crossovers to be matched'
        )
    parser.add_argument(
        '--return-slices', action='store_true',
        help='precise search : ensure strict temporal matching within the time '
             'window at the search. If not set, the temporal matching is made '
             'at granule level. Returns the slices of the granule subset '
             'matching the search criteria.'
         )
    parser.add_argument(
        '--show-each', action='store_true',
        help="display each found granule's foot print on a separate map"
    )
    parser.add_argument(
        '--show-all', action='store_true',
        help='display all found granule in one map'
    )
    parser.add_argument(
        '--output-format', type=str,
        help="format of the returned result, among: list, detailed, json"
        )


def decode_crosssearch_args(args):
    vargs = vars(args)

    return CrossSearchConfig(
        **{k: v for k, v in vargs.items() if v is not None})


def parse_args(cli_args):
    parser = argparse.ArgumentParser(
        description='Search the cross-overs between two indexes')

    add_crosssearch_args(parser)
    naiad.utils.config.add_verbosity_args(parser)
    naiad.utils.config.add_config_args(parser)
    Store.add_store_connection_args(parser)

    args = parser.parse_args(cli_args)

    # read defaults from config
    defaults = naiad.utils.config.decode_config_args(args)

    process_args = CrossSearchCommand(
        verbosity=naiad.utils.config.decode_verbosity_args(
            args, defaults.verbosity),
        crosssearch=decode_crosssearch_args(args),
        store=Store.decode_store_connection_args(args, defaults),
    )
    return process_args


def main(cli_args=None):
    args = parse_args(cli_args)

    # Set up verbosity option.
    naiad.cli.set_verbosity(args.verbosity.level)

    try:
        # instantiate Store object
        store = naiad.connect(
            args.store.store_type, **args.store.dict(), **args.verbosity.dict())

        vargs = args.crosssearch.dict()
        crossovers = store.crossover_search(
            reference=args.crosssearch.cross,
            crossed_index=args.crosssearch.versus,
            #area=shapely.geometry.box(*vargs.pop('area')),
            **vargs)

        #print(crossovers, len(crossovers))

    except StoreConnectionError as e:
        print(e.message)
        sys.exit(1)
    finally:
        store.close_connection()

    if args.crosssearch.output_format == naiad.cli.FormatEnum.list:
        for crossover in crossovers:
            print(crossover.reference.granule, crossover.match.granule)

    elif args.crosssearch.output_format == naiad.cli.FormatEnum.csv:
        for crossover in crossovers:
            print(crossover)

    elif args.crosssearch.output_format == naiad.cli.FormatEnum.detailed:
        for crossover in crossovers:
            print(crossover)

    elif args.crosssearch.output_format == naiad.cli.FormatEnum.json:
        print(json.dumps([_.geojson() for _ in crossovers], indent=4))

    sys.exit(0)

    # # init terminal colors
    # init()

# ==========================

    # if args.show or args.show_all or args.full_footprint:
    #     try:
    #         import matplotlib.pyplot as plt
    #     except:
    #         raise ImportError("matplotlib is missing")
    #
    # reference = args.cross.lower()
    # crossed = args.versus.lower().split(',')
    # if type(crossed) is not list:
    #     crossed = [crossed]
    #
    # if args.area:
    #     lonmin, latmin, lonmax, latmax = literal_eval(args.area)
    # else:
    #     # global selection by default
    #     lonmin, latmin, lonmax, latmax = -180, -90., 180., 90
    # area = shapely.geometry.asPolygon([
    #          (lonmin, latmax),
    #          (lonmax, latmax),
    #          (lonmax, latmin),
    #          (lonmin, latmin),
    #          (lonmin, latmax),
    #          ])
    # if args.start:
    #     start = dateutil.parser.parse(args.start)
    # else:
    #     start = datetime(1950, 1, 1)
    # if args.end:
    #     end = dateutil.parser.parse(args.end)
    # else:
    #     end = datetime.utcnow()
    # if args.precise:
    #     precise = True
    # else:
    #     precise = False
    # if args.method:
    #     method = args.method
    # else:
    #     method = "granule"
    # if method not in ['granule', 'tile']:
    #     logging.error("Invalid method name : %s", method)
    #     exit(-1)
    #
    # if args.granule_constraints:
    #     granule_constraints = []
    #     fields = args.granule_constraints.split(';')
    #     for item in fields:
    #         prop, oper, val = item.split(' ')
    #         if oper not in ['eq', 'lt', 'le', 'gt', 'ge']:
    #             raise Exception("Invalid constraint operator : %s", oper)
    #         granule_constraints.append((prop, oper, val))
    # else:
    #     granule_constraints = None
    #
    # # create Naiad server object to query
    # es = Server(args.url, login=args.login, password=args.password)
    #
    # time_window = timedelta(minutes=int(args.time_window))
    #
    # search = ColocationQuery(reference, crossed, area, start, end, time_window,
    #                          precise=precise, method=method,
    #                          granule_constraints=granule_constraints,
    #                          percent=args.crossover_percent,
    #                          fail_silently=args.fail_silently)
    # try:
    #     result = search.run(es)
    # except NaiadSomeFailedCrossOvers as err:
    #     print("Tiles in error")
    #     for t in json.loads(err.message):
    #         print("    {}".format(t))
    #     if not args.fail_silently:
    #         exit(-1)
    #
    # if len(result["error"]) > 0:
    #     logging.error(result["error"])
    #
    # if args.json:
    #     print(json.dumps(result, cls=TileEncoder, sort_keys=True,
    #                      indent=4, separators=(',', ': ')))
    #
    # else:
    #     prev_crossover = None
    #     prev_ref = None
    #     for i, ref in enumerate(result["data"]["reference"]):
    #         crossedgranule = result["data"]["crossed"][i]
    #         # display
    #         cur_crossover = (ref.granule, crossedgranule.granule)
    #
    #         if prev_ref != ref.granule:
    #             print(Back.BLUE + Style.BRIGHT + ref.granule + Style.RESET_ALL)
    #             print()
    #         prev_ref = ref.granule
    #         print(Back.GREEN + "Reference" + Style.RESET_ALL)
    #         print(Style.BRIGHT + "Name      : " + Style.RESET_ALL + Fore.GREEN +
    #               "%s" % ref.granule + Style.RESET_ALL)
    #         print(Style.BRIGHT + "Time range: " + Style.RESET_ALL +
    #               "%s\t" % ref.start + Style.BRIGHT + "to " + Style.RESET_ALL +
    #               "%s" % ref.end)
    #         if ref.ymin is not None:
    #             print(Style.BRIGHT + "Slice     : " + Style.RESET_ALL +
    #                   "%s" % ref.slice())
    #         print(Style.BRIGHT + "Geometry  :" + Style.RESET_ALL +
    #               "%s" % ref.geometry)
    #         print(Back.RED + "Crossover : " + Style.RESET_ALL)
    #         print(Style.BRIGHT + "Name      : " + Style.RESET_ALL + Fore.RED +
    #               "%s" % crossedgranule.granule + Style.RESET_ALL)
    #         print(Style.BRIGHT + "Time range: " + Style.RESET_ALL +
    #               "%s\t" % crossedgranule.start + Style.BRIGHT + "to " +
    #               Style.RESET_ALL + "%s" % crossedgranule.end)
    #         if crossedgranule.ymin is not None:
    #             print(Style.BRIGHT + "Slice     : " + Style.RESET_ALL +
    #                   "%s" % crossedgranule.slice())
    #         print(Style.BRIGHT + "Geometry  :" + Style.RESET_ALL +
    #               "%s" % crossedgranule.geometry)
    #         print("\n\n")
    #
    #         if args.full_footprint:
    #             # query full granules footprint
    #             if prev_crossover != cur_crossover:
    #
    #                 # get reference footprint
    #                 granule_search = GranuleInfoQuery(reference, ref.granule)
    #
    #                 fullrefgranule = granule_search.run(es)["data"]
    #                 GeoShape.draw(fullrefgranule.geometry, colour="green")
    #
    #                 # get crossover footprint
    #                 granule_search = GranuleInfoQuery(crossedgranule.dataset_id,
    #                                                   crossedgranule.granule)
    #                 fullcrossgranule = granule_search.run(es)["data"]
    #                 GeoShape.draw(fullcrossgranule.geometry, colour="red")
    #             GeoShape.draw(ref.geometry)
    #
    #         elif args.show:
    #             crossedgranule.show(clip=ref.geometry)
    #
    #         # display
    #         if ((i == len(result["data"]["reference"]) - 1 or
    #                 ref != result["data"]["reference"][i+1]) and
    #                 (args.full_footprint or args.show)):
    #             # display all the crossovers for the same granule pair at the
    #             # same time
    #             plt.show()
    #
    #         prev_crossover = cur_crossover
    #
    # # display all crossovers
    # if args.show_all:
    #     for i, ref in enumerate(result["data"]["reference"]):
    #         GeoShape.draw(ref.geometry, colour="green")
    #     plt.show()
