"""
Command line tool for storing index records in index store.

:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import argparse
import datetime
import glob
import logging
import sys
from pathlib import Path

from pydantic import BaseModel

import naiad
import naiad.utils.config as cfg
from naiad.stores.store import Store


class RegistrationCommand(BaseModel):

    index: str
    records: str
    registration:  naiad.stores.store.RegistrationConfig =  \
        naiad.stores.store.RegistrationConfig()
    store: naiad.stores.store.StoreConfig = naiad.stores.store.StoreConfig()
    verbosity: cfg.VerbosityConfig = cfg.VerbosityConfig()


def add_registration_args(parser):
    parser.add_argument(
        '--index', type=str, required=True,
        help='identifier of the dataset (must be lowercase)'
    )
    parser.add_argument(
        '--records', type=str, required=True,
        help='full path or pattern to the files containing the index records '
             'to be stored'
    )
    parser.add_argument(
        '--bulk', action='store_true',
        help=(
            'register all tile files at once. Achieves the best performance '
            'when indexing multiple files at the same time. The default "file '
            'by file" indexing will allow easier detection of badly shaped tile '
            'files. Bulk registration will hold all your tile files in memory '
            'before registering them in naiad.')
        )
    parser.add_argument(
        '--skip-index-check', action='store_true', dest='skip_index_check',
        help=(
            'skip index existence check (faster but be sure your index is '
            'created or it will create a default index with the wrong mapping')
        )
    parser.add_argument(
            '--granule-only', action='store_true', dest='granule_only',
            help='only registers the granule documents.'
            )


def parse_args(cli_args):
    parser = argparse.ArgumentParser(
        description=(
            'Register granules and tiles in a naiad index. Default '
            'arguments are taken from the local or provided config file but '
            'can be superseded with inline arguments')
    )

    # generic arguments
    add_registration_args(parser)
    cfg.add_verbosity_args(parser)
    cfg.add_config_args(parser)

    # store specific arguments for registration
    Store.add_registration_args(parser)

    args = parser.parse_args(cli_args)

    # read defaults from config
    defaults = cfg.decode_config_args(args)

    process_args = RegistrationCommand(
        index=args.index,
        records=args.records,
        verbosity=cfg.decode_verbosity_args(args, defaults.verbosity),
        registration=Store.decode_registration_args(args, defaults),
        store=Store.decode_store_connection_args(args, defaults),
    )

    return process_args


def main(cli_args=None):
    start_exec = datetime.datetime.now()

    args = parse_args(cli_args)

    # Set up verbosity option.
    naiad.cli.set_verbosity(args.verbosity.level)
    if args.verbosity.level == naiad.cli.VerbosityEnum.verbose:
        logging.debug(args)

    # instantiate Store object
    store = naiad.connect(
        args.store.store_type, **args.store.dict(), **args.verbosity.dict())

    tile_files = glob.glob(args.records)
    if len(tile_files) == 0:
        logging.warning("No tile files found.")
        exit(0)
    tile_files = [Path(_) for _ in tile_files]

    # check index existence
    if not args.registration.skip_index_check and not store.exists(args.index):
        logging.error('The index {} is not existing'.format(args.index))
        exit(1)

    # index
    errors = store.register_from_files(
        args.index, tile_files, **args.registration.dict())

    end_exec = datetime.datetime.now()
    if args.verbosity.benchmark:
        logging.info(
            "Total execution time: {} s"
            .format((end_exec - start_exec).total_seconds())
        )

    if len(errors) > 0:
        logging.error(
            "{} errors occurred while registering".format(len(errors))
        )
        for e in errors:
            logging.error(str(e))
        store.close_connection()
        sys.exit(1)

    store.close_connection()
    sys.exit(0)
