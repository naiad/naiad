"""
Display the content of a naiad csv tile file, created with `naiad-index`.

:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import matplotlib.pyplot as plt
from pydantic import BaseModel, Field
from pydantic_cli import run_and_exit

import naiad.data.georecord as gr


class Options(BaseModel):
    class Config:
        validate_all = True
        validate_assignment = True
        CLI_JSON_ENABLE = True

    tile_file: str = Field(
        ...,
        description="path to the naiad tile file",
        required=True,
        extras={"cli": ('-i', '--input-file')}
    )
    footprint_only: bool = Field(
        default=False,
        description="display only the global footprint without the tiles",
        extras={"cli": ('-f', '--footprint-only')}
    )
    tiles_only: bool = Field(
        False,
        description="display only the tiles",
        required=False,
        extras={"cli": ('-t', '--tiles-only')}
    )


def inspect(cli_args: Options) -> int:
    args = cli_args

    footprint, tiles = next(iter(gr.read_csv(
        args.tile_file, granule_only=args.footprint_only).items()))

    # show result
    fig = None
    if not args.tiles_only:
        fig = gr.GeoRecord.draw(footprint.geometry, colour="red")

    if not args.footprint_only:
        for tile in tiles:
            if fig is None:
                fig = gr.GeoRecord.draw(tile.geometry, colour="green")
            else:
                gr.GeoRecord.draw(tile.geometry, colour="green", ax=fig)

    plt.show()


def main():
    run_and_exit(
        Options, inspect,
        description="Display the content of a granule tile file",
        version='0.2.0')
