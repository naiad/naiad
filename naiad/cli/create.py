"""
Command line tool for the creation of a product index in Naiad.

:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import argparse
import logging
import sys

from pydantic import BaseModel

import naiad
import naiad.utils.config as cfg
from naiad.stores.store import Store, StoreConfig, StoreConnectionError

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class IndexCreationCommand(BaseModel):
    create_args: naiad.stores.store.IndexCreationConfig =  \
        naiad.stores.store.IndexCreationConfig()
    store: StoreConfig = StoreConfig()
    verbosity: cfg.VerbosityConfig = cfg.VerbosityConfig()


def add_create_args(parser):
    parser.add_argument(
        '--index', type=str, required=True,
        help='identifier of the dataset (must be lowercase)'
    )
    parser.add_argument(
        '--dims', type=lambda s: [i for i in s.split(',')],
        required=True,
        help='list (comma separated) of subsetting dimensions'
    )


def parse_args(cli_args):
    parser = argparse.ArgumentParser(description='Create a Naiad index')

    # generic arguments
    add_create_args(parser)
    cfg.add_verbosity_args(parser)
    cfg.add_config_args(parser)

    # store specific arguments for index creation
    Store.add_creation_args(parser)

    args = parser.parse_args(cli_args)

    # read defaults from config
    defaults = cfg.decode_config_args(args)

    process_args = IndexCreationCommand(
        create_args=Store.decode_creation_args(args, defaults),
        verbosity=cfg.decode_verbosity_args(args, defaults.verbosity),
        store=Store.decode_store_connection_args(args, defaults),
    )

    return process_args


def main(cli_args=None):
    args = parse_args(cli_args)

    # Set up verbosity option.
    naiad.cli.set_verbosity(args.verbosity.level)

    try:
        # instantiate Store object
        store = naiad.connect(
            args.store.store_type, **args.store.dict(), **args.verbosity.dict())

        store.create_index(
            args.create_args.index.lower(),
            dims=args.create_args.dims,
            **args.store.dict())

    except StoreConnectionError as e:
        print(e.message)
        sys.exit(1)

    finally:
        store.close_connection()

    sys.exit(0)
