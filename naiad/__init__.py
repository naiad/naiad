"""
helper functions
"""
import json
import logging
import sys
from typing import List, Tuple, Union, Dict
import yaml

import shapely.geometry

from naiad.env import STORES, get_config_file
import naiad.utils.config
from naiad.data.georecord import GeoRecord


__all__ = ['stores', 'data', 'cli']


def connect(store: str, **kwargs):
    """Connect to a Naiad index store

    Args:
        store:  the store class
    """
    try:
        return STORES[store].load()(**kwargs)
    except KeyError:
        raise TypeError("Can't find this store type: {}.")


def search(
        indexes: Union[str, List[str]],
        conf=None,
        **kwargs) -> Dict[str, List[GeoRecord]]:
    """helper function to search granules using a config file to get store
    information associated with the datasets.

    Args:
        indexes: dataset index(es) where to search granules from

    """
    conf = get_config_file(conf)

    if conf is None:
        raise IOError("No configuration found")
    logging.info("configuration file: {}".format(conf))

    with open(conf) as f:
        config = naiad.utils.config.NaiadConfig(
            **yaml.load(f, Loader=yaml.FullLoader))

    store = None
    for dataset in indexes:
        if dataset not in config.indexes:
            raise ValueError("Unknown dataset {} in configuration file {}"
                             .format(dataset, conf))

        dstore = config.stores[config.indexes[dataset].store]
        if store is None:
            store = dstore
        elif store != dstore:
            raise ValueError("Can not search in different stores in this "
                             "version.")

    # instantiate Store object
    store = naiad.connect(store.store_type, **store.dict(), **kwargs)

    # search
    try:
        total, res = store.search(indexes, **kwargs)
    except ConnectionError as _:
        print("Search can not be performed (store can not be reached)",
              file=sys.stderr)
        exit(1)
    except naiad.stores.store.IndexNotFound:
        print("The index for dataset(s) {} is not existing".format(indexes),
              file=sys.stderr)
        exit(1)

    return res


def show(
        granules: List[naiad.data.georecord.GeoRecord],
        clip: shapely.geometry = None):
    naiad.data.georecord.GeoRecord.show_all(granules, clip=clip)


def from_json(filename: str) -> List[Tuple['Tile', 'Tile']]:
    """
    Restore the content of a query saved in geojson

    Args:
        filename: path to the geojson file with query result

    Returns:

    """
    with open(filename, 'r') as f:
        result = json.load(f)

    if 'crossed' in result['data']:
        # crossover result
        return crossover_tuple(result)
