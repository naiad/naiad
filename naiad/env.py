import os
from pathlib import Path
import pkg_resources


STORES = {
        entry_point.name: entry_point
        for entry_point
        in pkg_resources.iter_entry_points('naiad.stores')
}


def get_config_file(config: Path = None):
    """Get the user Naiad configuration file.

    Args:
        config (str, optional): configuration file containing the definition of
            indexes in Naiad. If not provided, the will search for
            NAIAD_HOME env variable or by default in the default
            ``.naiad/defaults.yaml`` file in the user home directory.
    """
    if config is not None and not config.exists():
        raise IOError(
            'Naiad configuration file {} not found'.format(config)
        )

    if config is None:
        if 'NAIAD_HOME' in os.environ:
            config = Path(os.environ['NAIAD_HOME']) / 'defaults.yaml'
        else:
            # look in user home
            config = Path.home() / '.naiad/defaults.yaml'

    if not config.exists():
        return

    return config