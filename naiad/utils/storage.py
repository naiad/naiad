import logging

STORAGE_PARAMS = {
    'storage_path': None,
    'time_parser': None
}


def get_storage_config_from_file(index: str, config: str = None):
    """Get the parameters for a specific index.

    Args:
        config (str, optional): configuration file containing the definition of
            indexes in Naiad. If not provided, the will search for
            NAIAD_HOME env variable or by default in the default
            ``.naiad/indexing.yaml`` file in the user home directory.
    """
    if config is None:
        return STORAGE_PARAMS

    if index not in config['indexes']:
        raise ValueError('index {} missing in configuration file'.format(index))

    idxconfig = {**STORAGE_PARAMS, **config['indexes'][index]}

    return idxconfig


def get_storage_config(config, args):
    idxconfig = get_storage_config_from_file(args.index, config)

    logging.info("Parameters:")
    for param in STORAGE_PARAMS:
        if getattr(args, param) is not None:
            idxconfig[param] = getattr(args, param)
        logging.info('  {}: {}'.format(param, idxconfig[param]))

    return idxconfig