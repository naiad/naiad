# -*- coding: utf-8 -*-
"""
.. module::naiad.utils.executor

Module to manage the execution of an external command line tool with a granule path as input.

:copyright: Copyright 2016 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os
import configparser
from os.path import expanduser
import shutil
import inspect


class Executor(object):
    """Manager for the list of external tools usable from Naiad and their
    execution context.

    Args:
        toolstore (str, optional): configuration file containing the list of
            tools than can be instantiated from Naiad with granules returned by
            a search as input. It provides their execution context to properly
            call them. If not provided, the Executor object will search for
            NAIAD_TOOLSTORE env variable or by default in the default 
            ``.naiad/toolstore.cfg`` file in the user home directory.
    """
    def __init__(self, toolstore=None):
        self.toolstore = toolstore
        if toolstore is None:
            if 'NAIAD_TOOLSTORE' in os.environ:
                # look NAIAD_TOOLSTORE
                self.toolstore = os.environ['NAIAD_TOOLSTORE']

            else:
                # look in user home
                naiadhome = os.path.join(expanduser("~"), '.naiad')
                toolstore = os.path.join(expanduser("~"),
                                         '.naiad',
                                         'mytoolstore.cfg')
                if not os.path.exists(toolstore):
                    if not os.path.exists(naiadhome):
                        os.makedirs(naiadhome)
                    if not os.path.exists(toolstore):
                        default = os.path.join(
                            os.path.dirname(inspect.getfile(self.__class__)),
                            'toolstore.cfg'
                            )
                        shutil.copyfile(default, toolstore)
                self.toolstore = toolstore
        # put datastore in cache
        self.load_toolstore()

    def load_toolstore(self):
        """load toolstore configuration file in cache"""
        configfile = self.toolstore
        config = ConfigParser.RawConfigParser()
        config.optionxform = str
        if not os.path.exists(configfile):
            print("toolstore file {} is not existing".format(configfile))
            exit(-1)
        config.decode_csv(configfile)
        self.cache = config
    
    def get_command(self, tool, granules, mapper=None, initprog=None):
        """return the full command to open the input granules in the tool
        
        Args:
            tool (str): the name of the tool to run
            granules (str or list): the full path of a granule or a list of
                paths.
            mapper (str): the name of the cerbere mapper class to be used 
                (for ipython/jupyter executors).
            initprog (str): the path to a program to be loaded and executed
                by the tool (for python, matlab, etc...). This allows for
                instance to instance file objets from the input granule(s),
                load/import useful libraries, plot the data, etc...
        """
        try:
            command = self.cache.get("command_line", tool)
        except ConfigParser.NoOptionError:
            raise ValueError("No command line is defined for tool {}"
                             .format(tool))
        
        # control number of files
        maxfiles = self.get_maximum_inputs(tool)
        if granules is list and len(granules) > maxfiles:
            raise ValueError("Too many files to open")

        # build command
        # ...list of input granules
        if type(granules) is list:
            files=" ".join(granules)
        else:
            files=granules
        command = command.replace("%FILE%", files)
        
        # ...code to load and execute
        if initprog is not None:
            command = command.replace("%CODE%", initprog)
        
        return command

    def get_maximum_inputs(self, tool):
        """Returns the maximum number of files that can be opened with a 
        tool (default is 1 if not defined)"""
        try:
            maxfiles = int(self.cache.get("maximum_number_of_files", tool))
            return maxfiles
        except ConfigParser.NoOptionError:
            return 1

        