from pathlib import Path
from typing import Dict, Tuple, List, Any
import yaml

from pydantic import BaseModel

from naiad.cli import VerbosityEnum
import naiad.stores.store

# plugins for available index store engines
global STORES


class IndexProcessing(BaseModel):
    dataset_class: str = None
    feature_class: str = None
    steps: Dict[str, int] = None
    archive: bool = True
    tile_output: Path = None
    benchmark: bool = True
    bulk: bool = True


class IndexConfig(BaseModel):
    store: str
    registration:  naiad.stores.store.RegistrationConfig = \
        naiad.stores.store.RegistrationConfig()
    processing: IndexProcessing = None


class VerbosityConfig(BaseModel):
    level: VerbosityEnum = 'verbose'
    fail_silently: bool = False
    benchmark: bool = False


class NaiadConfig(BaseModel):
    stores: Dict[str, naiad.stores.store.StoreConfig] = []
    indexes: Dict[str, IndexConfig] = []
    verbosity: VerbosityConfig = VerbosityConfig()


def add_verbosity_args(parser):
    # Add verbosity control.
    verbosity_group = parser.add_mutually_exclusive_group()
    verbosity_group.add_argument(
        "-v", "--verbose", action="store_true",
        help="Activate debug level logging - for extra feedback."
    )
    verbosity_group.add_argument(
        "-q", "--quiet", action="store_true",
        help="Disable information logging - for reduced feedback."
    )
    verbosity_group.add_argument(
        "-s", "--silent", action="store_true",
        help="Log ONLY the most critical or fatal errors."
    )
    parser.add_argument(
        '--benchmark', action='store_true',
        help='provides the time elasped between start and end of execution'
        )
    parser.add_argument(
        "-f", "--fail_silently", action="store_true",
        help="ignore errors returned by some requests (will remove silently "
             "some results from a query if they raise an exception or error."
    )
    return verbosity_group


def decode_verbosity_args(args, config=None):
    # priority : args > config > class/processing defaults
    dargs = VerbosityConfig().dict()
    if config is not None:
        dargs.update(config.dict())

    dargs.update(
        {'benchmark': args.benchmark, 'fail_silently': args.fail_silently})

    vargs = vars(args)
    level = next(iter([_.value for _ in VerbosityEnum if vargs[_]]), None)
    if level is not None:
        dargs['level'] = level

    return VerbosityConfig(**dargs)


def get_connection_config(config, args=None, index=None):
    if config is None:
        connection = CONNECTION_PARAMS
    else:
        connection = config['globals']

    config = config['indexes']
    logging.info("Connection parameters:")
    for param in ['es_server', 'username', 'password', 'fail_silently']:
        if args is not None and getattr(args, param) is not None:
            connection[param] = getattr(args, param)
        elif index in config and param in config[index]:
            connection[param] = config[index][param]
        elif param not in connection:
            connection[param] = CONNECTION_PARAMS[param]
        logging.info('  {}: {}'.format(param, connection[param]))

    return connection


def get_multiconnection_config(indices):
    """get connection details for multiple indices"""
    if not isinstance(indices, list):
        raise TypeError('indices must be a list')
    connections = {}
    for index in indices:
        connections[index] = get_connection_config(
            naiad.env.get_config_file(), index=index
        )

    if len(set(frozenset(d.items()) for d in connections.values())) == 1:
        connection = list(connections.values())[0]
        return Server(
            connection['es_server'],
            login=connection['username'],
            password=connection['password']
        )

    return {
        index: Server(connection['es_server'],
                      login=connection['username'],
                      password=connection['password'])
        for index, connection in indices.items()
    }


def add_config_args(parser):
    parser.add_argument(
        '-c, --conf', type=Path, default=None, dest='conf',
        help='path to the configuration file if not using the default one'
    )


def decode_config_args(args):
    conf = naiad.env.get_config_file(args.conf)

    if conf is None:
        return NaiadConfig()

    with open(conf) as f:
        return NaiadConfig(**yaml.load(f, Loader=yaml.FullLoader))

