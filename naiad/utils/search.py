from ast import literal_eval
import datetime
from enum import Enum
import logging
from pathlib import Path
from pydantic import BaseModel
from typing import Dict, Tuple, List, Any

import dateutil
import shapely.geometry


SEARCH_PARAMS = {
    'area': shapely.geometry.box(-180., -90., 180., 90.),
    'start': datetime.datetime(1950, 1, 1),
    'end': datetime.datetime.utcnow(),
    'precise': False,
    'granule_constraints': None,
    'subset_constraints': None,
    'full_path': False,
    'output_format': 'list',
    'datastore': None,
    'toolstore': None,
    'exttool': None
}


def add_search_args(parser):
    parser.add_argument(
        '--area', type=str,
        help='area of interest, defined as "lonmin,latmin,lonmax,latmax"'
        )
    parser.add_argument(
        '--start', type=str,
        help='start time of the period'
        )
    parser.add_argument(
        '--end', type=str,
        help='end time of the period'
        )
    parser.add_argument(
        '--precise', action='store_true',
        help='precise search : ensure strict temporal matching within the time '
             'window at the search. If not set, the temporal matching is made '
             'at granule level. Returns the slices of the granule subset '
             'matching the search criteria.'
         )
    parser.add_argument(
        '--subset-constraints', type=str,
        help='specify some constraints on properties to subset the '
             'granules only on relevant content. Only the subsets '
             'of the granule matching the constraints will be '
             'returned.'
        )
    parser.add_argument(
        '--granule-constraints', type=str,
        help='specify some constraints on properties at granule '
             'level. Only the granules matching the specified '
             'constraints will return a result.'
        )
    parser.add_argument(
        '--full-path', action='store_true',
        help="return the matching granules with their full local "
             "name"
        )
    parser.add_argument(
        '--datastore', type=str,
        help='path to the datastore configuration file defining '
             'the access rules to the granule files. Optional arg '
             'and only applicable if --full-path is set.'
        )
    parser.add_argument(
        '--toolstore', type=str,
        help='path to the toolstore configuration file defining '
             'how to pipe the naiad search result in an external tool. '
             'Onl applicable if --exttool is set.'
        )
    parser.add_argument(
        '--exttool', type=str,
        help="name of the tool, configured in the toolstore "
             "configuration file, into which are piped the returned granules"
        )
    parser.add_argument(
        '--output-format', type=str,
        help="format of the returned result, among list, detailed, json"
        )


def get_search_config_from_file(index: str, config: Dict = None):
    """Get the parameters for a specific index.

    Args:
        config (str, optional): configuration file containing the definition of
            indexes in Naiad. If not provided, the will search for
            NAIAD_HOME env variable or by default in the default
            ``.naiad/indexing.yaml`` file in the user home directory.
    """
    if config is None:
        return SEARCH_PARAMS

    if index not in config['indexes']:
        logging.warning(
            'Index {} missing in configuration file. Using defaults.'
            .format(index)
        )
        return SEARCH_PARAMS

    idxconfig = {**SEARCH_PARAMS, **config['indexes'][index]}
    for param in ['es_server', 'username', 'password']:
        if param not in idxconfig:
            idxconfig[param] = config['globals'][param]

    return idxconfig


def decode_constraints(constraints):
    granule_constraints = []
    if constraints is None:
        return granule_constraints

    fields = constraints.split(';')
    for item in fields:
        prop, oper, val = item.split(' ')
        if oper not in ['eq', 'lt', 'le', 'gt', 'ge']:
            raise Exception("Invalid constraint operator : %s", oper)
        granule_constraints.append((prop, oper, val))

    return granule_constraints


def get_search_config(config, args):
    idxconfig = get_search_config_from_file(args.index, config)

    logging.info("Parameters:")
    for param in SEARCH_PARAMS:
        if getattr(args, param) is not None:
            idxconfig[param] = getattr(args, param)

    if not isinstance(idxconfig['start'], datetime.datetime):
        idxconfig['start'] = dateutil.parser.parse(idxconfig['start'])
    if not isinstance(idxconfig['end'], datetime.datetime):
        idxconfig['end'] = dateutil.parser.parse(idxconfig['end'])
    if not isinstance(idxconfig['area'], shapely.geometry.base.BaseGeometry):
        idxconfig['area'] = shapely.geometry.box(literal_eval(args.area))
    if not isinstance(idxconfig['granule_constraints'], list):
        idxconfig['granule_constraints'] = decode_constraints(
            idxconfig['granule_constraints']
        )

    for param in SEARCH_PARAMS:
        logging.info('  {}: {}'.format(param, idxconfig[param]))

    return idxconfig
