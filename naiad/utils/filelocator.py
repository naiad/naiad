# -*- coding: utf-8 -*-
"""
.. module::naiad.utils.filelocator

Class to locate granules indexed in Naiad

:copyright: Copyright 2016 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import configparser
import os
from pathlib import Path

from dateutil import parser


def get_full_path(granule, storage, index):
    try:
        path = storage['storage_path'][index]
    except KeyError:
        raise ValueError(
            "No storage location defined for index {}"
                .format(index)
        )
    try:
        extractor = storage['time_extractor'][index].replace('$FILE', granule)
    except KeyError:
        raise ValueError(
            "No specified mean of extracting reference time for granule ")

    date = parser.parse(eval(extractor))
    return Path(date.strftime(path)) / granule


class FileLocator(object):
    """Class to locate granule files indexed in Naiad.

    Args:
        config (str, optional): configuration file containing the path and
            structure of the file organization for each dataset indexed in
            Naiad. If not provided, the FileLocator object will search for
            NAIAD_HOME env variable or by default in the default
            ``.naiad/search.yaml`` file in the user home directory.
    """

    def __init__(self, config):
        self.config = configparser.RawConfigParser()
        self.config.read(config)

    def get_full_path(self, granule, dataset_id):
        return get_full_path(granule, self.config, dataset_id)
