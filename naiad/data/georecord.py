"""
.. module::naiad.ingestion.tile

Class to store a granule tile

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from ast import literal_eval
from collections import OrderedDict
from datetime import datetime
import hashlib
import logging
from pathlib import Path
from typing import Any, Optional, Union, List, Dict

from dateutil import parser
import numpy as np
import shapely.geometry
import shapely.ops
import shapely.wkt
try:
    import cartopy.crs as ccrs
    import cartopy.feature as cfeature
    import matplotlib.pyplot as plt
except:
    logging.warning(
        "matplotlib, or cartopy are missing. Using display functions"
        " will fail"
    )


def read_csv(filepath, granule_only=False):
    """open tile file and read records"""
    with open(filepath, 'r') as fp:
        granule = GeoRecord.decode_csv(fp.readline())

        # check the footprint shape is valid
        if not granule.geometry.is_valid:
            logging.warning("invalid granule in {}. skipped.".format(filepath))
            logging.debug(str(granule))
            return

        if granule_only:
            return granule

        tiles = []
        for line in fp:
            tile = Tile.decode_csv(line)
            if tile.geometry.is_valid:
                tiles.append(tile)
            else:
                logging.warning("an invalid tile was skipped.")
                logging.debug(str(tile))

            # correct clockwise multipolygons
            # tile.geometry = orient(tile.geometry)

        granule.subsets = tiles

    return granule


class Tile(object):
    """Properties of granule subset (tile)"""
    def __init__(
            self,
            geometry: shapely.geometry.base.BaseGeometry = None,
            start: Union[datetime, np.datetime64] = None,
            end: Union[datetime, np.datetime64] = None,
            slices: Dict[str, int] = None,
            properties: Dict[str, Any] = None):
        if not isinstance(
                geometry, (shapely.geometry.base.BaseGeometry, type(None))):
            raise TypeError("geometry is not a shapely object.")

        if not isinstance(slices, (dict, type(None))):
            raise TypeError("slices must be an OrderedDict")

        if not isinstance(properties, (dict, type(None))):
            raise TypeError("properties must be an OrderedDict")

        if isinstance(geometry, shapely.geometry.MultiPolygon):
            geometry = shapely.ops.unary_union([
                _.buffer(0.) for _ in geometry.geoms])
            # geometry.show()

        self.geometry = geometry

        # times
        self.start = start
        if isinstance(start, np.datetime64):
            self.start = datetime.utcfromtimestamp(
                start.astype('O')/1e9)

        self.end = end
        if isinstance(start, np.datetime64):
            self.end = datetime.utcfromtimestamp(
                end.astype('O')/1e9)

        self.properties = properties
        self.slices = slices

    def __str__(self):
        strtile = \
            f'Start time: {self.start}\n' \
            f'End time  : {self.end}\n' \
            f'Geometry  : {self.geometry}\n'

        if self.slices is not None:
            strtile += 'Slices    : {self.slices}\n'

        if self.properties is not None:
            strtile += 'Properties: {self.properties}\n'

        return strtile

    @classmethod
    def decode_csv(cls, line):
        line = line.replace(' nan,', ' None,')
        fields = line.split(';')
        if len(fields) == 5:
            raise ValueError(f'Not a Tile object. Missing fields in {fields}')

        # mandatory GeoRecord fields
        granule, startstr, endstr, polygon, feature = fields[0:5]
        rec = Tile(
            geometry=shapely.wkt.loads(polygon),
            start=parser.parse(startstr),
            end=parser.parse(endstr),
        )

        # manage old and new format
        idx = 5
        if type(literal_eval(fields[idx])) is not list:
            #  old format for slices
            slices = []
            while (idx < len(fields)
                   and type(literal_eval(fields[idx])) is not list):
                slices.append(int(fields[idx]))
                idx += 1
            rec.slices = cls._slices_from_csv(slices, feature)
        else:
            # in new format, next items should be slices and properties lists of
            # tuples
            rec.slices = OrderedDict(literal_eval(fields[idx]))
            idx += 1

        if idx == len(fields):
            return rec

        # properties
        rec.properties = OrderedDict(literal_eval(fields[idx]))

        return rec

    @staticmethod
    def _slices_from_csv(offsets, feature):
        """Return tile offsets as slices"""
        if feature == 'Trajectory':
            return OrderedDict([('time', slice(offsets[0], offsets[1]))])
        elif feature == 'Swath':
            return OrderedDict([
                ('row', slice(offsets[0], offsets[1])),
                ('cell', slice(offsets[2], offsets[3]))])
        elif feature == 'Grid':
            return OrderedDict([
                ('y', slice(offsets[0], offsets[1])),
                ('x', slice(offsets[2], offsets[3]))])
        elif feature == 'CylindricalGrid':
            return OrderedDict([
                ('lat', slice(offsets[0], offsets[1])),
                ('lon', slice(offsets[2], offsets[3]))])

    def geojson(self):
        """return the tile shape as a geojson python object"""
        geojson_area = shapely.geometry.mapping(self.geometry)
        geojson = {
            'geometry': geojson_area,
            'time_coverage_start': self.start.isoformat(),
            'time_coverage_end': self.end.isoformat(),
            "properties": self.properties,
        }

        if self.slices is not None:
            geojson['slices'] = {
                k: [v.start, v.stop] for k, v in self.slices.items()
            }

        return geojson

    def hash(self, granule):
        """return a  unique hash code identifying the tile"""
        m = hashlib.md5()
        m.update(granule.encode('utf-8'))
        for sl in self.slices.values():
            m.update(str(sl.start).encode('utf-8'))
            m.update(str(sl.stop).encode('utf-8'))

        return m.hexdigest()


class GeoRecord(Tile):
    """Class to store an observation feature.

    @TODO remove slices in GeoRecord

    Args:
        geometry (BaseGeometry): shape of the tile. Must be provided as a
            shapely object (Polygon, MultiPolygon, MultiLine).

    """
    def __init__(
            self,
            granule: str,
            feature: str,
            dataset_id: str = None,
            path: Path = None,
            geometry: shapely.geometry.base.BaseGeometry = None,
            start: Union[datetime, np.datetime64] = None,
            end: Union[datetime, np.datetime64] = None,
            slices: Dict[str, int] = None,
            properties: Dict[str, Any] = None,
            subsets: List[Tile] = None,
    ):
        super(GeoRecord, self).__init__(
            geometry=geometry, start=start, end=end, properties=properties,
            slices=slices
        )

        self.granule = granule
        self.dataset_id = dataset_id
        self.feature = feature
        self.path = path
        self.subsets = subsets

    def __str__(self):
        strtile = \
            f'Name      : {self.granule}\n' \
            f'Dataset   : {self.dataset_id}\n'
        if self.feature is not None:
            strtile += f'Feature   : {self.feature}\n'
        if self.path is not None:
            strtile += f'Path      : {self.path}\n'
        strtile += super(GeoRecord, self).__str__()

        if self.subsets is not None:
            for i, tile in enumerate(self.subsets):
                strtile += f'\nSubset [{i}]\n' + tile.__str__()

        strtile += '\n'

        return strtile

    def geojson(self):
        """return the tile shape as a geojson python object"""
        geojson = super(GeoRecord, self).geojson()
        geojson.update({
            'granule': self.granule,
            'feature': self.feature,
        })

        if self.subsets is not None:
            geojson['subsets'] = [_.geojson() for _ in self.subsets]

        return geojson

    def encode_csv(self):
        """Encode into a csv line"""
        line = ";".join([
            str(self.granule),
            self.start.strftime('%Y-%m-%dT%H:%M:%S'),
            self.end.strftime('%Y-%m-%dT%H:%M:%S'),
            str(self.geometry),
            self.feature if self.feature is not None else ''
        ])

        # slices
        if self.slices is not None:
            for k, v in self.slices.items():
                line = ";".join([str[v.start], str[v.end]])

        # properties
        if self.properties is not None and len(self.properties) > 0:
            properties = {}
            for prop, value in self.properties.items():
                if isinstance(value, datetime):
                    properties[prop] = value.strftime('%Y-%m-%dT%H:%M:%S')
                else:
                    properties[prop] = value
            line += ";%s" % properties
        #line += '\n'

        return line

    @classmethod
    def from_geojson(cls, obj: dict):
        kwargs = {}
        if 'feature' in obj:
            kwargs['feature'] = obj['feature']
        if 'slices' in obj:
            slices = eval(obj['slices'])
            if 'time' in slices:
                # trajectory
                kwargs['ymin'] = slices['time'].start
                kwargs['ymax'] = slices['time'].stop
            elif 'row' in slices and 'cell' in slices:
                # swath
                kwargs['ymin'] = slices['row'].start
                kwargs['ymax'] = slices['row'].stop
                kwargs['xmin'] = slices['cell'].start
                kwargs['xmax'] = slices['cell'].stop
            elif 'x' in slices and 'y' in slices:
                # grid
                kwargs['ymin'] = slices['y'].start
                kwargs['ymax'] = slices['y'].stop
                kwargs['xmin'] = slices['x'].start
                kwargs['xmax'] = slices['x'].stop
            else:
                raise ValueError('Unknown slices')

        return GeoRecord(
            obj["granule"],
            geometry=shapely.geometry.shape(obj["__geo_interface__"]),
            start=datetime.strptime(obj["start"], '%Y%m%dT%H%M%S'),
            end=datetime.strptime(obj["end"], '%Y%m%dT%H%M%S'),
            **kwargs
        )

    @classmethod
    def decode_csv(cls, line):
        line = line.replace(' nan,', ' None,')
        fields = line.split(';')

        # mandatory GeoRecord fields
        granule, startstr, endstr, polygon, feature = fields[0:5]
        rec = GeoRecord(
            granule.strip(),
            feature=feature.strip(),
            geometry=shapely.wkt.loads(polygon),
            start=parser.parse(startstr),
            end=parser.parse(endstr),
        )

        if len(fields) > 5:
            # properties
            rec.properties = OrderedDict(literal_eval(fields[5]))

        return rec

    @classmethod
    def draw(cls, geometry, colour="blue", ax=None):
        if ax is None:
            figure = plt.figure(figsize=(20, 20), constrained_layout=False)
            ax = figure.add_subplot(projection=ccrs.PlateCarree())
            land_50m = cfeature.NaturalEarthFeature(
                'physical', 'land', '50m',
                edgecolor='face', facecolor=cfeature.COLORS['land'])
            ax.add_feature(land_50m)
            ax.coastlines('50m')
            gl = ax.gridlines(draw_labels=True)
            gl.top_labels = False
            gl.left_labels = False

        lines = (shapely.geometry.LineString, shapely.geometry.MultiLineString)
        points = (shapely.geometry.Point, shapely.geometry.MultiPoint)
        if isinstance(geometry, lines):
            ax.add_geometries(
                [geometry], crs=ccrs.PlateCarree(),
                alpha=0.2, facecolor='none', edgecolor=colour, zorder=2
            )
        elif isinstance(geometry, points):
            ax.scatter(geometry.x, geometry.y, marker='+', color=colour)
        else:
            ax.add_geometries(
                [geometry], crs=ccrs.PlateCarree(),
                alpha=0.2, facecolor=colour, edgecolor=colour, zorder=2
            )

        return ax

    def show(self, clip=None):
        """Draw a shape on a map.

        Args:
            clip (shapely.geometry): another shape (like a selection area of
                a granule footprint to show in background.
        """
        ax = self.draw(self.geometry, colour="green")
        if clip:
            self.draw(clip, colour="red", ax=ax)
        plt.show()

    @classmethod
    def show_all(cls, tiles, clip=None):
        """Draw multiple shapes on a map.

        Args:
            clip (shapely.geometry): another shape (like a selection area of
                a granule footprint to show in background.
        """
        if len(tiles) == 0:
            logging.warning("No tiles to plot")
            return

        ax = cls.draw(tiles[0].geometry, colour="green")
        for tile in tiles[1:]:
            tile.draw(tile.geometry, colour="green", ax=ax)
        if clip:
            cls.draw(clip, colour="red", ax=ax)
        plt.show()

