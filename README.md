# naiad

A suite of backend tools in python for indexing, searching and
colocating EO data.

Refer to the documentation in 'docs' folder or at:

https://naiad.gitlab-pages.ifremer.fr/naiad/
