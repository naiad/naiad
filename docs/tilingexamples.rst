Examples of tiling for various products
=======================================

KNMI L2B products for ASCAT-A and ASCAT-B
-----------------------------------------

ASCAT has a double swath. This has to be specified or the resulting shape will
not separate both tiles.

 .. code-block:: bash

    naiad-tile --doubleswath=41 --xstep 41 --ystep 41 --orientation=counterclockwise "/home/cerdata/provider/knmi/satellite/metop-a/ascat/l2b/12.5km/2010/230/*.nc" KNMIL2NCFile --show


IASI L2P from EUMETSAT / OSI SAF
--------------------------------

These are short (3 minutes along-swath) granules. We don't want to tile them
along the 'row' axis (along-track axis). We don't specify any `ystep` argument.
The tiler will automatically adjust the tile to the granule row length.  

 .. code-block:: bash

    naiad-tile --xstep 40  --display foo GHRSSTNCFile 


GPM JAXA L2A
------------

 .. code-block:: bash

   naiad-tile --ystep 50 GPMCOR_KUR_1501211635_1808_005104_L2S_DU2_03B.h5 GPMHDF5File


AATSR L2P GHRSST
----------------

 .. code-block:: bash

    naiad-tile --ystep 512  ../test/20100819-ATS_NR_2P-UPA-L2P-ATS_NR__2PNPDE20100819_012520_000045312092_00131_44273_5834-v01.nc GHRSSTNCFile --show 


AVHRR L2P from OSI SAF
----------------------

 .. code-block:: bash

    naiad-tile --xstep 512 --ystep 540 --outpath 20100508-EUR-L2P_GHRSST-SSTsubskin-AVHRR_METOP_A-eumetsat_sstmgr_metop02_20100508_225503-v01.7-fv01.0.nc GHRSSTNCFile


PODAAC Rapidscat data
---------------------

Several issues exist with the Rapidscat BUFR data:
  * swaths are not complete and miss some sections, creating spatial 
    discontinuities in the file.
  * the lat/lon contain a lot of invalid data (nan).

 .. code-block:: bash

    naiad-tile --xstep=21 --ystep 20 --view="{'cell':slice(0,41)}" --tiling_method="corners"  --use_clustering --gap_threshold=10. --orientation=counterclockwise ../test/rapid_20150101_003748_iss____01546_o_250_ovw_l2.bufr KNMIScatBUFRFile --show


MODIS L2P GHRSST from PODAAC
----------------------------
The MODIS L2P have some issues with invalid lat/lon.

 .. code-block:: bash

   naiad-tile --xstep=452 --ystep=677 --tiling_method="corners" jpl-l2p-modis_a/data/2015/001/20150101-MODIS_A-JPL-L2P-A2015001000500.L2_LAC_GHRSST_N-v01.nc GHRSSTNCFile --show


SARAL L2 from EUMETSAT
----------------------

Saral altimeter data are along-track observations. Use the `Trajectory` feature
class, and ``--ystep`` argument to specify the size of the tiles.

 .. code-block:: bash

   naiad-tile ../test/data/saral/SRL_OPRSSHA_2PTS026_0007_20150806_112515_20150806_130348.EUM.nc Cryosat2NCFile --ystep 75 --show --feature Trajectory


AMSR2 L2P from REMSS
--------------------

 .. code-block:: bash

   naiad-tile --xstep 50 --ystep 50  --feature Swath --orientation=clockwise --show ../test/data/20150101000936-REMSS-L2P_GHRSST-SSTsubskin-AMSR2-l2b_v07.2_r13957-v02.0-fv01.0.nc GHRSSTNCFile

IASI 1C spectra data in NetCDF, EUMETSAT
----------------------------------------

.. code-block:: bash

   naiad-tile --xstep=40 --ystep=10 /media/sumba/Iomega_HDD/iasi/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,MetOpA+IASI_C_EUMP_20100702085353_19203_eps_o_l1.nc  NCFile --show

.. image:: images/tiles/iasi_l1c_spectra.png



