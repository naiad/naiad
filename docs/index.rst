.. naiad documentation master file, created by
   sphinx-quickstart on Sun Aug  9 16:45:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Naiad : indexing and searching earth observation data
*****************************************************

**Naiad** is a free software solution, written in python, whose aim is to
provide Earth Observation users with an open-source, flexible and reusable tool
to index and search within earth observation dataset the relevant data
subsets matching user defined criteria. It is also used to detect cross-overs
between different observation sources.

Credits
=======

**Naiad** is developed by `Ifremer/CERSAT`_.

.. _Ifremer/CERSAT: https://www.ifremer.fr


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: For users

   Getting started <getting-started-guide/index.rst>
   User guide <user-guide/index>


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Orientation

   orientation


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Examples

   tilingexamples


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: For developers/contributors

   GitLab repository <https://gitlab.ifremer.fr/naiad/naiad>


License¶
========
**Naiad** is available under the open source `GPLv3 license`__.

__ https://www.gnu.org/licenses/gpl-3.0.en.html

