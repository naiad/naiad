Orientation
===========

Orientation applies to swath and gridded data (two dimensional). This is used to ensure proper tile and bounding area definitions, which are ambiguous on a sphere.

A polygonal shape is said clockwise if the ring of vertices that defines it is ordered clockwise. A product is natively clockwise if its data (or a subset) of its data are such that the polygon defined by its corners (row, cell), (row, cell + dx), (row + dy, cell + dx), (row + dy, cell), (row, cell) is ordered clockwise (when seen on a cartesian space).
By convention, geographical shapes must be defined counterclockwise for data contained within the external ring.

Note that when crossing the date line, a subset of a natively counterclockwise product will be ordered clockwise : this property is used to detect date line crossing.
