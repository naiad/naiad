.. _git repository: https://gitlab.ifremer.fr/naiad/naiad.git

==========
Quickstart
==========

**Naiad** is a framework to perform fast multi-criteria searches over earth
observation data, as well as to detect cross-overs between different
observations from different platforms (such as satellites).

Note all the command line tools used below can be replaced with direct calls
to python functions within a script, using the :ref:`Naiad python API<Naiad
API>`.

Indexing data
=============

To search for earth observation data, these data first need to be *indexed*.
An **index** is the metadata repository where we store the properties of the
registered earth observation data files (**granules**), including their spatial
shape (or footprint) and temporal extent (start and end time of measurements).

As a starter we use AVHRR L2P METOP product by OSI SAF, which contains sea
surface temperature data measured by polar orbiting satellites. Index records
have been prepared for this example, in the
``samples/tiles/avhrr_sst_metop_a-osisaf-l2p-v1.0/`` folder of the `git
repository`_ of ``naiad`` package.

To know more about the content of index records and create similar records for
your own datasets, refer to :ref:`EO data indexing<Data indexing in Naiad>`
section.


Creating and populating a product index
---------------------------------------
Metadata repositories can be implemented through on the shelf frameworks able to
store and query spatial geometry objects, such as Elasticsearch. Here we
assume an Elasticsearch v7 (``ES7``) instance is available in your
local processing environment (with its default URL http://localhost:9200).

To first create a product index, use the ``naiad-create-index`` command:

.. code-block:: bash

  naiad-create-index ES7 --url http://localhost:9200/ avhrr_sst_metop_a-osisaf-l2p-v1.0 row,cell


The two main arguments are the the product index name
(``avhrr_sst_metop_a-osisaf-l2p-v1.0``) and the temporal or spatial dimensions
(``row,cell``) along which the input files were segmented along (go to
:ref:`EO data indexing<Data indexing in Naiad>` for more details).

Depending on where or how your Elasticsearch cluster is installed, you may
need to use here a different URL, provide login and password, etc... (check
``naiad-create-index ES7 --help`` to see all the options available for
Easticsearch 7).

Then to register the provided index records into the index we just created,
simply ingest the content of these index record files with ``naiad-register``
command. Go to the folder where you downloaded the index record files and run:

.. code-block:: bash
 
  naiad-register ES7 --url http://localhost:9200 avhrr_sst_metop_a-osisaf-l2p-v1.0 "test/samples/tiles/avhrr_sst_metop_a-osisaf-l2p-v1.0/*.tiles"


You have now data indexed into Elasticsearch.


Get a granule description and display
-------------------------------------
We can get the description of a previously registered granule, for instance
*20150719233103-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_A-sstmgr_metop02_20150719_233103-v02.0-fv01.0.nc.tiles*
and also display its shape on a map using ``naiad-inquire`` command:

.. code-block:: bash
   
   naiad-inquire ES7 --url http://localhost:9200 20150719233103-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_A-sstmgr_metop02_20150719_233103-v02.0-fv01.0.nc.tiles --show

Do a spatio-temporal search
---------------------------
Naiad is mainly about spatio-temporal search, so let's search for some granules
intersecting an area and period of interest. This is done with
``naiad-search`` command.

Get everything! (be careful!):

.. code-block:: bash

   naiad-search ES7 --url http://localhost:9200 avhrr_sst_metop_a-osisaf-l2p-v1.0 --show

Add a time frame restriction:

.. code-block:: bash

   naiad-search ES7 --url http://localhost:9200 avhrr_sst_metop_a-osisaf-l2p-v1.0 --start 2010-08-19 --end 2010-08-20 --show

And now some area, defined as lonmin, latmin, lonmax, latmax:

.. code-block:: bash

   naiad-search ES7 --url http://localhost:9200 avhrr_sst_metop_a-osisaf-l2p-v1.0 --start 2010-08-19 --end 2010-12-20 --area='-20,-20,20,20' --show

You can request several products at the same time using a coma-separated
list, for instance assuming here we have a second product index called
`ascat_a`:

.. code-block:: bash

   naiad-search ES7 --url http://localhost:9200 ascat_a,avhrr_sst_metop_a-osisaf-l2p-v1.0  --start 2010-08-19 --end 2010-12-20 --area='-20,-20,20,20' --show

You can specify some constraints on the granule properties, if they have been
registered in the product index. For instance, get all granules with a
`file_quality_level` greater than 3 (if such a property was defined).

.. code-block:: bash

	naiad-search amsr2 --granule_constraints=file_quality_level.ge.3

Multiple constraints can be combined, separating the different constraints by a ``;``:

.. code-block:: bash

	naiad-search amsr2 --granule_constraints='file_quality_level.ge.3;file_quality_level.lt.3'


Do a cross search (colocation)
------------------------------
We want to keep only the granules from different products that cross each other
within a time window.

One product, given by ``--cross`` argument is considered as the "reference"
sensor, the one with which all other products, given by ``--versus``
argument must match within the time window given by ``--time_window``:

.. code-block:: bash

   naiad-cross-search --cross amsr2 --versus srl_oprssha_2 --time_window=720

You can display the crossovers one by one with ``--show`` option:

.. code-block:: bash

   naiad-cross-search --cross amsr2 --versus srl_oprssha_2 --time_window=720 --show

.. image:: ../images/crossover.png

Use ``--full_footprint`` option in addition to display the full footprint of
the colocated granules(usefull for checking).

.. code-block:: bash

   naiad-cross-search --cross amsr2 --versus srl_oprssha_2 --time_window=720 --full_footprint --show

.. image:: ../images/crossover_fullprint.png



Use filters in search
---------------------

If some properties were indexed together with the granules or the tiles, they can be used
as search filters, using either ``--granule_constraints`` to filter on granule properties.
For instance let's assume we gave indexed the 
granules of *slstr* product with a property *version* specifying the algorithm version used
for a specific granule. We can query the granules having *version* number equal to 2.3:

.. code-block:: bash

   naiad-search slstr  --granule_constraints='version eq 2.3'

Note the available comparison operators: ``eq``, ``lt``, ``le``, ``gt``, ``ge``.

Filters can be combined (logical AND) using ``;``:
 
.. code-block:: bash

   naiad-search slstr  --granule_constraints='version eq 2.3';'cloud_fraction le 0.5'


It can be used also in a cross-over search, but the expressed constraint (for now) only
applies to the reference dataset, defined by ``--cross argument``. In the following example,
we request the cross-overs between *slstr* and *metop_a* datasets where the *version* property
for *slstr* granules equals 2.3:

.. code-block:: bash

   naiad-cross-search --cross slstr --versus metop_a --time_window=15--start 20160520T000000 --end 20160610T000000  --granule_constraints='version eq 2.3'
 
 
Some more commands
==================
 
Delete an index
---------------
This will delete the index and all registered granules and tiles. Use with caution.
 
 .. code-block:: bash
 
    curl -XDELETE http://localhost:9200/avhrr_sst_metop_a-osisa-l2p-v1.0/



