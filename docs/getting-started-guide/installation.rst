.. _Elasticsearch: https://www.elastic.co/elasticsearch/

============
Installation
============

**Naiad** python API allows to **index** Earth Observation data into
a search engine such as `Elasticsearch`_  and to perform then **search
queries** on these indexes:

``naiad`` package is available at:
https://gitlab.ifremer.fr/naiad/naiad.git

A complementary package, ``naiad-index``, can be used to help you create the
index files from Earth Observation data, in the format expected by **Naiad**. It
can be obtained at: https://gitlab.ifremer.fr/naiad/naiad-index.git


Prerequisites
=============

One of the following solutions as index/search engine:

- Elasticsearch 7


Elasticsearch_ is a distributed, Java based, RESTful search and analytics engine
that is used in *Naiad* to index EO data as geometries. This is what allows
later to perform fast spatial and temporal search queries on these indexed data.

To install an Elasticsearch, refer to Elasticsearch_ online documentation.

Elasticsearch can be installed on one or multiple nodes for better performances.
The number of required node will depend on the amount of EO data that need to be
indexed, and the expected performances of your search queries.

Configure Elasticsearch following their instructions and your hardware
configuration then start it as a service:

.. sourcecode:: bash

  sudo service elasticsearch start


Installing with pip
===================

It is recommended to install **Naiad** in a virtualenv or conda environment.

to install ``naiad`` package with pip, simply run:

.. code-block:: bash

  pip install git+https://gitlab.ifremer.fr/naiad/naiad.git@master


Installing with conda
=====================

First create your conda environment:

.. code-block:: bash

  conda create -n naiad python=3

Then install ``naiad`` package from ``conda-forge`` repo:

.. code-block:: bash

  conda install naiad



