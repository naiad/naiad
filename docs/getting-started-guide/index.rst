###############
Getting Started
###############

The getting started guide aims to get you using **Naiad** productively as
quickly as possible. It is designed as an entry point for new users, and it
provides an introduction to **Naiad**'s main concepts.

.. toctree::
   :maxdepth: 2
   :hidden:

   installation
   quick-overview
