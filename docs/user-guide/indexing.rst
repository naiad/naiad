======================
Data indexing in Naiad
======================

What is indexing?
-----------------

Searching for Earth Observation (EO) data of interest is based in **Naiad** on
the pre-indexing of these data as **geospatial features**, defined by:

* the spatial boundaries (geometrical shape) covered by the feature
* the time frame of the observations contained within the feature
* the data object (file) containing the observations associated with the feature
  and the slicing information (if only a subset of a source data file is
  indexed, these are the dimensions along which this subset was sliced from
  the source data file)
* any custom property labelling or describing the content of the feature,
  usable as a future search/selection criteria.

A feature may be a satellite image or swath or part of a swath, a trajectory or
part of a trajectory, a grid or part of a grid, a series of locations,...
corresponding to a data **granule** - a data structure stored as a whole in a
file, a database record, etc...

Features are recorded within a **geospatial index**, corresponding to a given
thematic and/or source product: it can be the indexing of a complete satellite
file collection, or the indexing of subsets matching a specific interest (storm
observations, etc...).

**Naiad** allows two levels of indexing in a single index:

* the **granule** level, providing the geospatial footprint and temporal
  interval covered by the whole feature
* optionally, the **segment** level subdividing the main feature into smaller
  subsets (**segment**). The **segment** level indexing is not required and is
  mainly used when dealing with large features (such as full satellite orbits,
  usually longer than 100 minutes) if fine temporal search is required later
  (with a precision of few minutes) or when one wants to add specific properties
  on the data content along this orbit. Using the **segment** level index will
  make an index much larger.

This is illustrated in the picture below showing the index record for a full
orbit file: the surface in green corresponds to a **granule** level
index record, bounding the whole footprint of the swath, while the **segment**
level records correspond to each sub-box in black. The **segment** records allow
later for better precision in temporal search and to return the slices
of the orbit file subset framing the user area of interest.

.. image:: images/swath_indexing.png


The index records to be registered into a **Naiad** index must follow a
specific format, if registering them from files with command ``naiad-register``,
or be stored in ``GeoRecord`` class objects if registering them within a
script with :ref:`Naiad python API<NaiadAPI>`.

Creating the index records can be done your own way or helped by using the
complementary ``naiad-index`` package, available at:
https://gitlab.ifremer.fr/naiad/naiad-index.git

Format of feature records
-------------------------




Creating index records
----------------------

As mentioned above, index records to be registered into **Naiad** search
engine can be created externally through your own scripting or using
``naiad-index`` package, as long as they fit the expected format of
``GeoRecord`` class structure.

However, note that in order for **Naiad** to work properly, the created index
records shall provide a geometrical shape (provided as a WKT string in index
file format, or a shapely geometry in `GeoRecord`` objects) following some
properties:

  * longitudes must be expressed between -180 and 180 degrees.
  * polygonal shapes shall be oriented counterclockwise

