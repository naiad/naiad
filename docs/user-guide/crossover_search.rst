===================================
Naiad cross-overs search in details
===================================

Naiad was especially designed to find cross-overs between different satellite
products. This section describes in more details how it can be done both with
the command line tool and the python API. It explores in more details the
available options.


naiad-cross-search command-line tool
====================================

``naiad-cross-search`` behaves in many ways as naiad-search command with some
additional options to adjust the colocation criteria for the crossover search.

The product against which crossovers are searched is specified with argument
``--cross`` whereas the crossed dataset(s) are specified with argument ``--versus``.

In addition to that, you need to specify a colocation window, expressed in minutes
with option ``time_window``. Other options for time frame or area are similar to the
ones used by ``naiad-search`` tool.

.. code-block:: bash

   naiad-cross-search --cross test_iasi_xxx_1c_m02 --versus test_iasi_xxx_1c_m02 --start 20161104 --end 20161106 --time_window 5

To keep the most relevant crossovers, you can also specify the minimum
intersection percentage with the reference granule, using ``--crossover-percent``
argument:

.. code-block:: bash

   naiad-cross-search --cross test_iasi_xxx_1c_m02 --versus test_iasi_xxx_1c_m02 --start 20161104 --end 20161106 --time_window 5 --crossover-percent 50

