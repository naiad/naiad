=======================
Naiad search in details
=======================


Naiad is mainly about spatio-temporal search, but finer search on some specific
properties registered with the granule footprint and time coverage can also be
performed.

This section details how to search granules both with the command line tool and
the python API. It explores in more details the available options.

naiad-search command-line tool
==============================

So let's search for some granules intersecting an area and period of interest. 
This is done with ``naiad-search`` command.

All you need to know first is the name of the index you want to search your product
granules from. Here we use an index called ``test_pdgs_sl_2_wct____nr`` in which we
registered SLSTR L1 granules from sentinel-3.

Get all options with:

.. code-block:: bash

   naiad-search --help


Selection options
-----------------

Get everything! (be careful!):

.. code-block:: bash

   naiad-search test_pdgs_sl_2_wct____nr


Add a time frame restriction:

.. code-block:: bash

   naiad-search test_pdgs_sl_2_wct____nr --start 2016-11-09 --end 2016-11-10


And now some area, defined as lonmin, latmin, lonmax, latmax:

.. code-block:: bash

   naiad-search test_pdgs_sl_2_wct____nr --start 2016-11-09 --end 2016-11-10 --area='-20,-20,20,20'


You can request several products at the same time using a coma-separated list:

.. code-block:: bash

   naiad-search test_pdgs_sl_2_wct____nr,test_pdgs_sl_2_wst____nr  --start 2016-11-19 --end 2016-11-20 --area='-20,-20,20,20'

You can specify some constraints on the granule properties, if they have been
registered in the product index.

For instance, get all granules with a cloud coverage (``slstr_class_summary_1km_cloudy_region_pix`` property)
lower than 10%:

.. code-block:: bash

    naiad-search test_pdgs_sl_2_wct____nr --granule_constraints='slstr_class_summary_1km_cloudy_region_pix le 10' --start 2016-11-09 --end 2016-11-10


Multiple constraints can be combined, separating the different constraints by a ``;``,
here for instance testing both cloud and land coverage:

.. code-block:: bash

    naiad-search test_pdgs_sl_2_wct____nr --granule_constraints='slstr_class_summary_1km_cloudy_region_pix le 10;slstr_class_summary_1km_land_pix le 50' --start 2016-11-09 --end 2016-11-10


Format options
--------------

If Naiad has been properly configured, you can get the full path to the selected
granules with ``--full-path`` option:

.. code-block:: bash

   naiad-search test_pdgs_sl_2_wct____nr --granule_constraints='slstr_class_summary_1km_cloudy_region_pix le 10;slstr_class_summary_1km_land_pix le 50' --start 2016-11-09 --end 2016-11-10 --full-path


``--show`` option allows you to check the location of each selected granules on a map:

.. code-block:: bash

   naiad-search test_pdgs_sl_2_wct____nr --granule_constraints='slstr_class_summary_1km_cloudy_region_pix le 10;slstr_class_summary_1km_land_pix le 50' --start 2016-11-09 --end 2016-11-10 --show


``--output-format json`` will return the selected granules as a json list:

.. code-block:: bash

   naiad-search test_pdgs_sl_2_wct____nr --granule_constraints='slstr_class_summary_1km_cloudy_region_pix le 10;slstr_class_summary_1km_land_pix le 50' --start 2016-11-09 --end 2016-11-10 --output-format json





