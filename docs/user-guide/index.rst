##########
User Guide
##########

In this user guide, you will find a detailed description of **Naiad** concepts
and examples that describe many common tasks that you can accomplish.

.. toctree::
   :maxdepth: 2
   :hidden:

   indexing
   naiad_search
   crossover_search
   api
   configuration
   customization
   datastore
   granule_and_tiles
   server