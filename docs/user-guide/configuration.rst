=============
Configuration
=============

The usage of **Naiad** API can be simplified using a local configuration file
that stores the properties of the frequently sought indices.

This local configuration file must be named ``indices.yaml`` and stored in
a ``.naiad`` directory, preferably in your home dir.

Environment variables
=====================

A few environment variables can be set to make usage of Naiad more convenient:

  * ``NAIAD_ELASTICSEARCH`` avoids having to specify the elasticsearch host to
    send a Naiad query to. It can be a single host or a pool of elasticsearch
    master nodes.
    
    For instance, for a single host:

.. code-block:: bash

   export NAIAD_ELASTICSEARCH=http://eumetsat-gses-4:9200   
    
    Or for a pool of hosts:

.. code-block:: bash

   export NAIAD_ELASTICSEARCH="['http://eumetsat-gses-4:9200','http://eumetsat-gses-5:9200','http://eumetsat-gses-6:9200']"


By doing so, you won't have anymore to specify the URL of the default Naiad server
to which you send search queries.

For instance, in command line mode:

.. code-block:: bash


Or in a python script:

.. code-block:: python





Access to data files
====================

Naiad provides a handy class, :py:mod:`datastore` to get the full path to granules
returned by a Naiad search. For this to work, you need to define some information
on the physical location of your datasets in a configuration file stored in your
home directory: ``$HOME/.naiad/mydatastore.cfg``.

Here is an example of such configuration file:


You can then retrieve the full path to the granule returned by a Naiad query, as
shown in the example below:

.. code-block:: python




  
