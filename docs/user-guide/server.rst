============
Naiad server
============

Overview
========

Naiad uses ElasticSearch framework to index and query the EO data ingested in
the system. This framework must be pre-installed to use Naiad.

Local installation
------------------

If Naiad is fully installed locally on a standalone machine (meaning ElasticSearch
is also installed on this machine), using the default ElasticSearch port (9200),
then there is no need to specify the url of an ElasticSearch node when
running a search or creating an index. Naiad will automatically use the default
port for ElasticSearch.

For instance, for creating an index called ``test``, just type in (instead of
explicitly specifying an ElasticSearch node with ``--elasticsearch``):

.. code-block:: bash

   naiad-create-index toto

The ``--elasticsearch`` does not need to be used in any of Naiad command line
tools, unless you install ElasticSearch on a different port. For install, if
ElasticSearch was installed on port 9201, you will have to type in:

.. code-block:: bash

   naiad-create-index --elasticsearch http://localhost:9201 toto


Nor need an explicit ``server`` keyword to be used in an API call:

.. code-block:: python

   # compose the query
   search = SpatioTemporalSearch(products, area, start, end,
                                 granule_constraints=constraints)
    
   # execute the query (no need here for specifying server keyword)
   res = search.run()




Distributed environment
------------------------
